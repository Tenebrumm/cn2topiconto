# -*- coding: utf-8 -*-
from owlready2 import *
import readline

class MyCompleter(object):  # Custom completer

    def __init__(self, options):
        self.options = sorted(options)

    def complete(self, text, state):
        if state == 0:  # on first trigger, build possible matches
            if text:  # cache matches (entries that start with entered text)
                self.matches = [s for s in self.options 
                                    if s and s.startswith(text)]
            else:  # no text entered, all matches possible
                self.matches = self.options[:]

        # return match indexed by state
        try: 
            return self.matches[state]
        except IndexError:
            return None


def suggest_relation(onto):
    relations = ["IsA"]

    with onto:
        for r in list(onto.object_properties()):
            relations.append(r.name)

    completer_rel = MyCompleter(relations)
    readline.set_completer(completer_rel.complete)
    readline.parse_and_bind('tab: complete')

    rel = input("\nrelation name: ")
    return rel
    
def suggest_options(c, rel, options_list, *inverse):
    completer_opt = MyCompleter(options_list)
    readline.set_completer(completer_opt.complete)
    readline.parse_and_bind('tab: complete')
    if inverse:
        con = input("x."+ rel + "." + c + ", x = ")
    else:
        con = input(c + "." + rel + ".") 
    return con

def suggest_subclasses(c, some_list):
    completer_sub = MyCompleter(some_list)
    readline.set_completer(completer_sub.complete)
    readline.parse_and_bind('tab: complete')

    subs = input("Enter subclass(es) for " + c + ": ") 
    return subs
   
def suggest_superclasses(some_list):
    completer_sup = MyCompleter(some_list)
    readline.set_completer(completer_sup.complete)
    readline.parse_and_bind('tab: complete')

    sups = input("Enter superclass(es): ") 
    return sups
   
def suggest_instances(c, options_list):
    completer_ins = MyCompleter(options_list)
    readline.set_completer(completer_ins.complete)
    readline.parse_and_bind('tab: complete')

    ins = input("Enter instances for " + c + ": ")
    return ins
    
def suggest_stuff(options_list, printline):
    completer_stuff = MyCompleter(options_list)
    readline.set_completer(completer_stuff.complete)
    readline.parse_and_bind('tab: complete')

    stuff = input(printline)
    return stuff