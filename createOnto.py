# -*- coding: utf-8 -*-

import settings, file_modification, request
from cleanupOnto import remove_Thing
from owlready2 import *
import types, itertools
import taxonomy

def create(onto):
    fName_save = settings.fName_tax

    # input
    concept = settings.name # CN label
    conceptid = settings.namecid # CN id
    conceptoclass = settings.nameoclass # classname
    conceptdef = [conceptid, concept, conceptoclass] # definition of input concept
    
    # if new ontology
    if not list(onto.classes()):
        # creates user entered concept as subclass of Thing
        create_class(onto, conceptoclass)
        
        # a dictonary that collects options for relations
        # key is each oclass and it contains a list of all added tuples
        dic_dump = {}
        # a dictonary that links oclass and CN id information of each created oclass
        dic_tax_map = {conceptoclass:conceptid}
    
    # existing ontology loaded
    else:
        print("Existing taxonomy was loaded. Please choose the selected first level again. The second level will then" \
        " be prompted only if no respective sub- or superconcepts exist yet.")
        try:
            dic_tax_map = file_modification.read_dic_from_file(settings.fName_dic_tax_map)
        except FileNotFoundError:
            print("There seems to be no map file. Exiting...")
            raise SystemExit
        try:
            dic_dump = file_modification.read_dic_from_file(settings.fName_dic_dump) 
        except FileNotFoundError:
            print("There seems to be no dump file. Exiting...")
            raise SystemExit
        
    # direct of user entered concept
    tax = expand_tax_for_concept(onto, conceptdef, dic_tax_map, False, True) # recommended: no nountag, related
    create_taxonomy(onto, tax, dic_tax_map)
    dic_dump[tax.oclass] = [x for x in tax.dump]
    
    file_modification.save_in_file(onto, fName_save)
    file_modification.save_dic_in_file(dic_dump, settings.fName_dic_dump)
    file_modification.save_dic_in_file(dic_tax_map, settings.fName_dic_tax_map)

    # second level sublist
    if tax.subl:
        for sub in tax.subl:
            print("----------------------------------------------")
            if not onto[sub[2]].subclasses() or get_direct_superclasses(onto, sub[2], True) == [onto[conceptoclass]]:
                subtax = expand_tax_for_concept(onto, sub, dic_tax_map, True, False) # recommended: nountag, no related
                create_taxonomy(onto, subtax, dic_tax_map)
                dic_dump[subtax.oclass] = [x for x in subtax.dump] 
                
                file_modification.save_in_file(onto, fName_save)          
                file_modification.save_dic_in_file(dic_dump, settings.fName_dic_dump)
                file_modification.save_dic_in_file(dic_tax_map, settings.fName_dic_tax_map)
     
    # second level suplist
    if tax.supl:
        for sup in tax.supl:
            print("----------------------------------------------")
            if not get_direct_superclasses(onto, sup[2], True) or get_subclasses(onto, sup[2]) == [onto[conceptoclass]]:
                suptax = expand_tax_for_concept(onto, sup, dic_tax_map, True, False) # recommended: nountag, no related
                create_taxonomy(onto, suptax, dic_tax_map)
                dic_dump[suptax.oclass] = [x for x in suptax.dump]  
                  
                file_modification.save_in_file(onto, fName_save)     
                file_modification.save_dic_in_file(dic_dump, settings.fName_dic_dump)
                file_modification.save_dic_in_file(dic_tax_map, settings.fName_dic_tax_map) 
            
    # link unlinked concepts
    modify(onto, conceptoclass)
            
    # reasoning to clarify
    with onto:
        # if there was anything but the main concept created
        if len(list(onto.classes())) > 1:
            sync_reasoner()
        else:
            print("\nIt seems like there was not nothing added for %s. \n Please restart or choose a different topic concept." % settings.name)
            file_modification.rm_file(settings.fName_tax)
            file_modification.rm_file(settings.fName_dic_tax_map)                        
            raise SystemExit
    
    # save dictinaries and taxonomy
    file_modification.save_dic_in_file(dic_dump, settings.fName_dic_dump)
    file_modification.save_dic_in_file(dic_tax_map, settings.fName_dic_tax_map)
    
    print("SAVING")
    file_modification.save_in_file(onto, fName_save)

# creates Sub and Super in onto as specified in tax    
def create_taxonomy(onto, tax, dic_tax_names):
    with onto:
        # remove Thing which causes problems
        search = 'Thing'
        for e in tax.subl:
            if e[2] == search:
                tax.subl.remove(e)   
        for e in tax.supl:
            if e[2] == search:
                tax.supl.remove(e)  
        
        setsubl = set([x[2] for x in tax.subl])
        setsupl = set([x[2] for x in tax.supl])
        intersection = (setsubl).intersection(setsupl)
        
        if intersection:
            # make class equivalent
            for x in intersection:
                create_class(onto, x)
                if onto[tax.oclass] not in onto[x].equivalent_to:
                    onto[x].equivalent_to.append(onto[tax.oclass])
                    onto[tax.oclass].equivalent_to.append(onto[x])
                    
            # delete from lists
            for x in tax.subl:
                if x[2] in intersection:
                    tax.subl.remove(x)
            for x in tax.supl:
                if x[2] in intersection:
                    tax.supl.remove(x)
                    
        #try:
        # subclasses
        if tax.subl:
            for s in tax.subl:
                # concept does not exist yet
                if s[2] not in [x.name for x in onto.classes()]: 
                    NewClass = types.new_class(s[2], (onto[tax.oclass],))
                    dic_tax_names[s[2]] = s[0] 
                # concept exists
                else:
                    if not check_inheritance_cycle(onto, s[2], tax.oclass):
                        onto[s[2]].is_a.append(onto[tax.oclass])
        # superclasses
        if tax.supl:
            for s in tax.supl:
                # concept does not exist yet
                if s[2] not in [x.name for x in onto.classes()]:
                    create_superclass_of_sub(onto, tax.oclass, s[2])
                    dic_tax_names[s[2]] = s[0]
                # concept exists
                else:
                    if not check_inheritance_cycle(onto, tax.oclass, s[2]):
                        onto[tax.oclass].is_a.append(onto[s[2]])
                        
#        except TypeError:
#            print("Looks like an inheritance cylce was caused. Unfortunately this is not permitted. Exiting.")
#            raise SystemExit
             
                
            
# expands a concept triple [id, label, oclass] and creates the taxonomy part
def expand_tax_for_concept(onto, concept, dic_tax_names, nountag, related):
    tax = taxonomy.TaxSubSup(onto, concept)
    tax.add_subclasses(nountag)
    tax.add_superclasses(nountag)
    tax.add_name_oclasses()
    tax.verify(dic_tax_names)
    if related:
        tax.add_related_to()
    
    return tax

# link unconnected sub- and superclasses 
def modify(onto, conceptoclass):
    import userInput, userInputTax
    # make links (doubleoutput, no output)
    with onto:
        # keeps track of lists to not ask the same concepts twice
        tracking_list = []
        # link superclasses of sub- and superclasses
        for sub in get_subclasses(onto, conceptoclass)+get_direct_superclasses(onto, conceptoclass, True)+[onto[conceptoclass]]:
            if len(get_superclasses(onto, sub.name)) >= 4: # here Thing is still checked
                open_list = []
        
                # remove already previously linked classes
                for (first, second) in itertools.combinations(get_superclasses(onto, sub.name), 2):
                    if not second in get_superclasses(onto, first.name) and not first in get_superclasses(onto, second.name): # not linked yet
                        if first not in open_list:
                            open_list.append(first)
                        if second not in open_list:
                            open_list.append(second)

                
                if open_list and not check_with_tracking_list(open_list, tracking_list):
                    tracking_list.append(open_list)
                    print("-----------------------------------------------")
                    # chose indices and link        
                    userInput.print_list_with_index([x.name for x in open_list])
                    
                    for (first, second) in itertools.combinations(open_list, 2):
                        if second in get_superclasses(onto, first.name): # already linked
                            print(first.name, " is already a descendant of ", second.name)
                        if first in get_superclasses(onto, second.name): # already linked
                            print(second.name, " is already a descendant of ", first.name)
                    print("\n")
                    while userInput.query_yes_no("Link concepts?"):
                        
                        first, second = userInputTax.choose_move(open_list)
                        # nothing was entered
                        if first == "" or second == "":
                            pass
                        else:
                            # remove previous (now redundant) links
                            for x in list(set(first.descendants()) & set(second.descendants())):
                                if (x.name != first) and (x.name != second) and (x in second.subclasses()):
                                    print("%s superclass of %s removed" %(second, x))                                    
                                    x.is_a.remove(second)
                            
                            # create new link only if they are not descendants of eachother
                            if not first in second.descendants() and not second in first.descendants():
                                print(first)
                                print(second.descendants())
                                first.is_a.append(second)
                            # ask equivalent if other direction is present
                            elif second in first.descendants():
                                if userInput.query_yes_no("%s is already a superclass of %s, do you want to make them equivalent?" % (second.name, first.name)):
                                    first.equivalent_to.append(second)
                                else:
                                    print("Axiom \"%s SubsetOf of %s\" was not created." % (first.name, second.name))
                            
                            

# returns true if all elements in open_list have already been stored at some point in tracking_list
def check_with_tracking_list(open_list, tracking_list):
    for l in tracking_list:
        if all(x in l for x in open_list):
            return True
    return False

def check_inheritance_cycle(onto, subc, superc):
    from userRelations import handle_cycle    
    
    with onto:
        if onto[subc] and onto[superc] in onto.classes():
            if onto[superc] in onto[subc].descendants():
                handle_cycle(onto, subc, superc)
                # no cycle was created
                if not onto[subc] in onto[superc].equivalent_to:
                    print("Axiom \"%s IsA %s\" was not created." % (subc, superc))
                return True
            else:
                return False
                
#==============================================================================
# Basic Ontology Modifications
# Creating Classes, Adding/Getting Sup/Super Classes
#==============================================================================

# creates new class (subclass of Thing)
def create_class(onto, c):
    with onto:
        if c not in list(onto.classes()):
            NewClass = types.new_class(c, (Thing,))
    
        remove_Thing(onto)
            
# does not contain the class itself and Thing (if not separately added)                
def get_direct_superclasses(onto, sub, *with_filter):
    with onto:
        # if with_filter is true, then Thing and the concept itself are removed
        if with_filter:
            new_list = []
            for x in onto[sub].is_a:
                if x is not owl["Thing"] and type(x) is entity.ThingClass:
                    new_list.append(x)
            return new_list
            
        if sub == "Thing":
            return []
            
        else:
            return onto[sub].is_a

def get_superclasses(onto, sub, *with_filter):
    with onto:
        if sub == "Thing":
            return []
        else:
            # if with_filter is true, then Thing and the concept itself are removed
            if with_filter:
                new_list = []
                for x in list(onto[sub].ancestors()):
                    if x is not owl["Thing"] and x is not onto[sub]:
                        new_list.append(x)
                # remove equivalent
                for e in onto[sub].equivalent_to:
                    if e in new_list:
                        new_list.remove(e)
                return new_list
            else:
                return list(onto[sub].ancestors())

def get_subclasses(onto, sup):
    with onto:
        return onto[sup].subclasses()
        
# creates new subclass of a given superclass
def create_subclass_of_super(onto, sub, sup):
    with onto:
        # if the superclass does not exits yet, create it as subclass of Thing
        if sup not in list(onto.classes()):
            NewClass = types.new_class(sup, (Thing,))
        
        if not check_inheritance_cycle(onto, sub, sup):
            NewClass = types.new_class(sub, (onto[sup],))

        remove_Thing(onto)
        
# creates new superclass of a given subclass
def create_superclass_of_sub(onto, sub, sup):
    with onto:
        # if the superclass does not exits yet, create it as subclass of Thing
        if sup not in list(onto.classes()):
            NewClass = types.new_class(sup, (Thing,))
        
        # create subclass
        if not check_inheritance_cycle(onto, sub, sup):
            NewClass = types.new_class(sub, (onto[sup],))
        
        remove_Thing(onto)
        
# adds some superclass for an existing sub
def add_superclass_of_sub(onto, sub, sup):
    with onto:
        onto[sub].is_a.append(onto[sup])
        
#==============================================================================
# Basic Methods to Print and CLear and existing Ontology
#==============================================================================

def clear(onto):    
    for i in list(onto.classes()):
        destroy_entity(i)
        
def oprint(onto):
    print("Classes") 
    print(list(onto.classes()))
    try:
        for c in list(onto.classes()):
            if len(c.subclasses()):
                print("Subclasses of", c)
                print(c.subclasses())
#            print("Superclasses of", c )
#            print(c.ancestors())
    except AttributeError:
        None    


    
