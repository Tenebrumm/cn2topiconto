# -*- coding: utf-8 -*-
import sys
import settings
import json
from owlready2 import *

from createOnto import create
from genericRelations import GR
from userRelations import UR
from cleanupOnto import nest_classes
from file_modification import read_dic_from_file

def set_tc():
    import re
    import request
    from userInput import query_yes_no
    # enter TC and search for respetive entity in CN
    while True:
        print("Please set a topic concept. If associated files already exist, they will be loaded.")
        tc_input = input("Enter topic concept: ")
        tc_input = tc_input.lower()
        
        if tc_input == 'exit':
            raise SystemExit
            
        # try to find existing file
        for fname in os.listdir(settings.dName):
            if fname.endswith('_tax.owl') and all(w in fname for w in tc_input.split()):
                print("File was found.")
                # mapping exists
                for dname in os.listdir(settings.dName):
                    if fname.endswith('_dic_tax_map.json'):
                        dic_map = read_dic_from_file(fname)
                settings.namecid = '/c/%s/' % settings.language + fname.replace('_tax.owl','')
                settings.name = settings.namecid.replace('/c/%s/' % settings.language,'' )
                settings.nameoclass = settings.name.title().replace("_", "")
                settings.set_files()
                return

        # search for CNid
        incl_words = re.findall(r'\b\w+\b', tc_input)
        n_search = '+'.join(incl_words)
        r = request.CNRequest(None, 5, None, None, n_search)
        obj = r.get_obj()        
        try:            
            r_test = request.CNRequest(obj['@id'], 1, None, 1, None)
            obj_test = r_test.get_obj()
            try: 
                obj_test['error']
                print("No entity was found. Please try something else.")
                
            except KeyError as e:
                if query_yes_no("Store %s as respective CN id?" % obj['@id']):
                    settings.namecid = obj['@id']
                    if settings.namecid.startswith("/c/%s" % settings.language):
                        settings.name = settings.namecid.replace('/c/%s/' % settings.language,'' )
                    else:
                        print("The name could not be properly set. Please enter the name by hand or restart.")
                        settings.name = input()
                    settings.nameoclass = settings.name.title().replace("_", "")
                    settings.set_files()
                    return
                    
        except KeyError as e:
            print("No entity was found. Please try something else.")

def build_onto(start = 1, end = 5):
    from userInput import query_yes_no
    set_tc()
    
    if start == 1 and end >= 1:
        if query_yes_no("Do you want to start fresh?"):
            # create new ontology
            onto = get_ontology("http://test.org/onto.owl")
            
        else:
            try:
                print("Loading old taxonomy from %s" % settings.fName_tax)            
                onto = get_ontology("file://%s" % settings.fName_tax).load()
            except FileNotFoundError:
                print("File could not be loaded. No existing taxonomy file.")
                raise SystemExit
            
        # step 1: build taxonomy
        print("\n-----------------------------------------")
        print("           Step 1: Taxonomy              ")
        print("-----------------------------------------")
        create(onto)
        
    if start <= 2 and end >= 2:
        try:
            onto = get_ontology("file://%s" % settings.fName_tax).load()
        except FileNotFoundError:
            print("There seems to be no taxonomy file. Please do Step 1 first. Exiting.")
            raise SystemExit
        try:
            onto = get_ontology("file://%s" % settings.fName_GR).load()
            print("There already exists a GR file. This has been loaded. If you want to start Step 2 fresh, delete it first.")
        except FileNotFoundError:
            pass
        
        # step 2: general relations
        print("\n-----------------------------------------")
        print("        Step 2: General Relations         ")
        print("-----------------------------------------")
        GR(onto)
        
    if start <= 3 and end >= 3:
        # load
        try:
            onto = get_ontology("file://%s" % settings.fName_UR).load()
            print("There already exists a UR file. This is loaded now. If you want to start Step 3 fresh, delete it first.")            
        except FileNotFoundError:                
            try:
                onto = get_ontology("file://%s" % settings.fName_GR).load()
            except FileNotFoundError:
                print("There seems to be no GR file. We advise you to do Step 2 first. Loading basic taxonomy.")
                onto = get_ontology("file://%s" % settings.fName_tax).load()
            
        # step 2: user specified relations
        print("\n-----------------------------------------")
        print("   Step 3: User Specified Relations      ")
        print("-----------------------------------------")
        UR(onto)
        
    if start <= 4 and end >= 4:
        # load
        try: 
            onto = get_ontology("file://%s" % settings.fName_clean).load()
            print("There already exists a final file. This is loaded now. If you want to start Step 4 fresh, delete it first.")            
        except FileNotFoundError:
            try:
                onto = get_ontology("file://%s" % settings.fName_UR).load()
            except FileNotFoundError:
                try:
                    print("There seems to be no UR file. Loading GR file.")
                    onto = get_ontology("file://%s" % settings.fName_GR).load()
                except FileNotFoundError:
                    print("There seems to be neither a GR file nor a UR file. Please add axioms in Step 2 or Step 3. Exiting.")
                    raise SystemExit

        # final step: nest top level classes
        print("\n-----------------------------------------")
        print("    Step 4: Nesting and Clean Up         ")
        print("-----------------------------------------")
        nest_classes(onto, settings.fName_clean)
        
        print("\n\n---------------------------------------------------------------")
        print("Congratulations! You are done.")
        print("Your final .owl file can be found at %s." % settings.fName_clean)
    
if __name__ == "__main__":    
    from userInput import query_yes_no
    # command line arguments
    try:
        start = int(sys.argv[1])
        try:
            end = int(sys.argv[2])
            print("Starting run.\n")
            build_onto(start, end)
            
        except IndexError:
            print("Starting run.\n")
            build_onto(start)       

        
    except IndexError:
        print("Starting complete run.\n")
        build_onto()
    
    # print statistics
    print("--------------------------------------------------------")
    if query_yes_no("Do you want to see some statistics?"):
        # print role names
        onto = get_ontology("file://%s" % settings.fName_clean).load()
        with onto:
            print("---------------- ROLE NAMES ---------------------")
            print(', '.join([x.name for x in list(onto.object_properties())]))
        # print dict size
        print("---------------- KG-mapping ---------------------")
        dic_map = json.load(open(settings.fName_dic_tax_map))
        print("Map Size:", len(dic_map))       
            


    
