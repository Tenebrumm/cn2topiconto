# -*- coding: utf-8 -*-
from owlready2 import *
import settings, file_modification
from userInput import query_yes_no
from genericRelations import ClassQueue
import itertools

def suggest_type(onto, dic_tax_map, dic_dump, type_sug, fName_save, direction, rel_type):
    from relHasPart import ask_create_HasPart
        
    dic_names = file_modification.read_dic_from_file(settings.fName_dic_tax_map)
    # storage for all concepts that should be deleted for future suggestions 
    deletion_dic = {}
    # storage for all concepts that were used for an axiom higher up and are removed from suggestions
    used_dic = {}
    # storage for options that could be further specified
    specify_dic = {}
    
    already_asked = []
    mc = onto[settings.nameoclass]
    
    # generate classqueue
    q = ClassQueue()
    class_list = q.return_class_queue(onto, dic_tax_map)
    # print(class_list)
    if direction == "left":
        suggestions_dic = type_sug.suggestions_dic_left
    elif direction == "right":
        suggestions_dic = type_sug.suggestions_dic_right           
    
    for c in [x for x in class_list]:
        if onto[c] not in already_asked:
            if onto[c] in list(mc.ancestors())+list(mc.subclasses()) or \
             onto[c] in list(itertools.chain.from_iterable([anc.subclasses() for anc in mc.ancestors()])) and onto[c] not in mc.ancestors() or \
             onto[c] in list(itertools.chain.from_iterable(sub.ancestors() for sub in mc.subclasses())):
                
                # if any suggestions could be built
                if c in suggestions_dic:
                    # build initial lists
                    del_list, used_list, spec_list, restricted_sug_list = type_sug.restrict_suggestions(onto, c, deletion_dic, used_dic, specify_dic, direction) 
                    # if suggestions remain ask
                    if spec_list+restricted_sug_list:
                        if query_yes_no("\nDo you want suggestions (%s) to add axioms for %s?" % (rel_type,c)):
                            # ask user, save result
                            if direction == "left":
                                print_suggestions_left(c, restricted_sug_list, spec_list, rel_type)
                            elif direction == "right":
                                print_suggestions_right(c, restricted_sug_list, spec_list, rel_type)
            
                            # ask and update del_dic
                            deletion_dic = ask_throw_out(onto, c, deletion_dic, suggestions_dic)
                            already_asked.append(onto[c])
                            # update lists
                            del_list, used_list, spec_list, restricted_sug_list = type_sug.restrict_suggestions(onto, c, deletion_dic, used_dic, specify_dic, direction) 
                            # ask for relations to create and update spec_dic and used_dic
                            if rel_type == "parts":
                                used_dic, specify_dic = ask_create_HasPart(onto, c, used_dic, specify_dic, restricted_sug_list, spec_list, direction, fName_save)
                                if query_yes_no("Do you instead want to use a different role name for one of the suggestions?"):                        
                                    used_dic, specify_dic = ask_create_relation(onto, c, used_dic, specify_dic, restricted_sug_list, spec_list, dic_names, suggestions_dic,  fName_save)
                            else:
                                used_dic, specify_dic = ask_create_relation(onto, c, used_dic, specify_dic, restricted_sug_list, spec_list, dic_names, suggestions_dic,  fName_save)
                        else:
                            already_asked.append(onto[c])
                    else:
                        # print("Seems like no suggestions remain for %s." % c)
                        already_asked.append(onto[c])
                else:
                    # print("Seems like no suggestions could be found for %s." % c)
                    already_asked.append(onto[c])
                    
                # specifically ask if the user wants to go deeper for everything that is a direct subclass of the main class
                if onto[c] in list(mc.subclasses()):
                    already_asked = ask_for_subclasses(onto, mc, onto[c], dic_tax_map, dic_dump, type_sug, fName_save, already_asked, suggestions_dic, deletion_dic, used_dic, specify_dic, direction, rel_type)
                 
            
#            # ancestor or direct subclass of mc
#            if onto[c] in list(mc.ancestors())+list(mc.subclasses()):
#                # build initial lists
#                del_list, used_list, spec_list, restricted_sug_list = type_sug.restrict_suggestions(onto, c, deletion_dic, used_dic, specify_dic, direction) 
#                # ask user, save result
#                if c in suggestions_dic:
#                    if direction == "left":
#                        print_suggestions_left(c, restricted_sug_list, spec_list, rel_type)
#                    elif direction == "right":
#                        print_suggestions_right(c, restricted_sug_list, spec_list, rel_type)
#
#                    # ask and update del_dic
#                    deletion_dic = ask_throw_out(onto, c, deletion_dic, suggestions_dic)
#                    already_asked.append(onto[c])
#                    # update lists
#                    del_list, used_list, spec_list, restricted_sug_list = type_sug.restrict_suggestions(onto, c, deletion_dic, used_dic, specify_dic, direction) 
#                    # ask for relations to create and update spec_dic and used_dic
#                    # TODO: add here to only do this if still suggestions in specify dic or used dic
#                    if rel_type == "parts":
#                        used_dic, specify_dic = ask_create_HasPart(onto, c, used_dic, specify_dic, restricted_sug_list, spec_list, direction, fName_save)
#                        if query_yes_no("Do you instead want to use a different relation for one of the suggestions?"):                        
#                            used_dic, specify_dic = ask_create_relation(onto, c, used_dic, specify_dic, restricted_sug_list, spec_list, dic_names, suggestions_dic,  fName_save)
#                    else:
#                        used_dic, specify_dic = ask_create_relation(onto, c, used_dic, specify_dic, restricted_sug_list, spec_list, dic_names, suggestions_dic,  fName_save)
#                
#                else:
#                    print("Seems like no suggestions could be found for %s." % c)
#                    already_asked.append(onto[c])
#                    
#                # specifically ask if the user wants to go deeper for everything that is a direct subclass of the main class
#                if onto[c] in list(mc.subclasses()):
#                    already_asked = ask_for_subclasses(onto, mc, onto[c], dic_tax_map, dic_dump, type_sug, fName_save, already_asked, suggestions_dic, deletion_dic, used_dic, specify_dic, direction, rel_type)
#                
#            # a subclass of an ancestor of mc
#            elif onto[c] in list(itertools.chain.from_iterable([anc.subclasses() for anc in mc.ancestors()])) and onto[c] not in mc.ancestors():
#                # build initial lists
#                del_list, used_list, spec_list, restricted_sug_list = type_sug.restrict_suggestions(onto, c, deletion_dic, used_dic, specify_dic, direction)                 
#                # ask user, save result
#                if c in suggestions_dic:
#                    if direction == "left":
#                        print_suggestions_left(c, restricted_sug_list, spec_list, rel_type)
#                    elif direction == "right":
#                        print_suggestions_right(c, restricted_sug_list, spec_list, rel_type)
#                    already_asked.append(onto[c])
#                    # update lists
#                    del_list, used_list, spec_list, restricted_sug_list = type_sug.restrict_suggestions(onto, c, deletion_dic, used_dic, specify_dic, direction)                     
#                    # ask for relations to create and update spec_dic and used_dic
#                    if rel_type == "parts":
#                        used_dic, specify_dic = ask_create_HasPart(onto, c, used_dic, specify_dic, restricted_sug_list, spec_list, direction, fName_save)
#                        if query_yes_no("Do you instead want to use a different relation for one of the suggestions?"):                        
#                            used_dic, specify_dic = ask_create_relation(onto, c, used_dic, specify_dic, restricted_sug_list, spec_list, dic_names, suggestions_dic,  fName_save)                                   
#                    else:
#                        used_dic, specify_dic = ask_create_relation(onto, c, used_dic, specify_dic, restricted_sug_list, spec_list, dic_names, suggestions_dic,  fName_save)                                   
#                    
#                    # specifically ask if the user wants to go deeper for further subclasses                    
#                    already_asked = ask_for_subclasses(onto, mc, onto[c], dic_tax_map, dic_dump, type_sug, fName_save, already_asked, suggestions_dic, deletion_dic, used_dic, specify_dic, direction, rel_type)
#            
#            # ancestor of a subclass of mc
#            elif onto[c] in list(itertools.chain.from_iterable(sub.ancestors() for sub in mc.subclasses())):
#                # build initial lists
#                del_list, used_list, spec_list, restricted_sug_list = type_sug.restrict_suggestions(onto, c, deletion_dic, used_dic, specify_dic, direction) 
#                # ask user, save result
#                if c in suggestions_dic:
#                    if direction == "left":
#                        print_suggestions_left(c, restricted_sug_list, spec_list, rel_type)
#                    elif direction == "right":
#                        print_suggestions_right(c, restricted_sug_list, spec_list, rel_type)                
#                    already_asked.append(onto[c])
#                    # update lists
#                    del_list, used_list, spec_list, restricted_sug_list = type_sug.restrict_suggestions(onto, c, deletion_dic, used_dic, specify_dic, direction) 
#                    # ask for relations to create and update spec_dic and used_dic
#                    if rel_type == "parts":
#                        used_dic, specify_dic = ask_create_HasPart(onto, c, used_dic, specify_dic, restricted_sug_list, spec_list, direction, fName_save)
#                        if query_yes_no("Do you instead want to use a different relation for one of the suggestions?"):                                                
#                            used_dic, specify_dic = ask_create_relation(onto, c, used_dic, specify_dic, restricted_sug_list, spec_list, dic_names, suggestions_dic,  fName_save)                        
#
#                    else:
#                        used_dic, specify_dic = ask_create_relation(onto, c, used_dic, specify_dic, restricted_sug_list, spec_list, dic_names, suggestions_dic,  fName_save)
#                                    
#                    # specifically ask if the user wants to go deeper for further subclasses                    
#                    already_asked = ask_for_subclasses(onto, mc, onto[c], dic_tax_map, dic_dump, type_sug, fName_save, already_asked, suggestions_dic, deletion_dic, used_dic, specify_dic, direction, rel_type)                 
#    
    print("\n")             
    file_modification.save_in_file(onto, fName_save)

def ask_for_subclasses(onto, mc, c, dic_tax_map, dic_dump, type_sug, fName_save, already_asked, suggestions_dic, deletion_dic, used_dic, specify_dic, direction, rel_type):
    from relHasPart import ask_create_HasPart
    dic_names = file_modification.read_dic_from_file(settings.fName_dic_tax_map) 
     
    if any(csub not in list(mc.ancestors())+list(mc.subclasses()) \
        and csub not in list(itertools.chain.from_iterable([anc.subclasses() for anc in mc.ancestors()])) \
        and csub not in list(itertools.chain.from_iterable(sub.ancestors() for sub in mc.subclasses())) \
        and csub.name in dic_tax_map \
        for csub in c.subclasses()):
        
        possible_subclasses = []
        possible_subclasses.extend([csub for csub in c.descendants() if \
        (csub != c \
        and csub not in list(mc.ancestors())+list(mc.subclasses()) \
        and csub not in list(itertools.chain.from_iterable([anc.subclasses() for anc in mc.ancestors()])) \
        and csub not in list(itertools.chain.from_iterable(sub.ancestors() for sub in mc.subclasses())) \
        and csub.name in dic_tax_map)])
        
        # everything was already asked
        if all(csub in already_asked for csub in possible_subclasses):
            return already_asked
        # check if descendants have suggestions
        if any([csub.name in suggestions_dic for csub in possible_subclasses]):
            pass
        else:
            # TODO: print choice
            # print("No suggestions exist for any further descendants (%s)." % ", ".join(map(str, [x.name for x in possible_subclasses])))
            already_asked.extend([x for x in possible_subclasses])
            return already_asked

        if query_yes_no("\nThere are further subclasses of %s with suggestions. Do you want to see those?" %c.name):
            print("\n>>> Entering subclasses of %s." % c.name)
            for csub in list(c.subclasses()):
                if not csub in already_asked:
                    # if any suggestions could be built
                    if csub.name in suggestions_dic:
                        # build initial lists
                        del_list, used_list, spec_list, restricted_sug_list = type_sug.restrict_suggestions(onto, csub.name, deletion_dic, used_dic, specify_dic, direction) 
                        # if suggestions remain ask
                        if spec_list+restricted_sug_list:
                            if query_yes_no("\nDo you want suggestions (%s) to add axioms for %s?" % (rel_type,csub.name)):
                                # ask user, save result                           
                                if direction == "left":
                                    print_suggestions_left(csub.name, restricted_sug_list, spec_list, rel_type)
                                elif direction == "right":
                                    print_suggestions_right(csub.name, restricted_sug_list, spec_list, rel_type)
                
                                already_asked.append(csub)
                        
                                del_list, used_list, spec_list, restricted_sug_list = type_sug.restrict_suggestions(onto, csub.name, deletion_dic, used_dic, specify_dic, direction) 
                                # ask for relations to create and update spec_dic and used_dic
                                if rel_type == "parts":
                                    used_dic, specify_dic = ask_create_HasPart(onto, csub.name, used_dic, specify_dic, restricted_sug_list, spec_list, direction, fName_save)
                                    if query_yes_no("Do you instead want to use a different role name for one of the suggestions?"):                        
                                        used_dic, specify_dic = ask_create_relation(onto, csub.name, used_dic, specify_dic, restricted_sug_list, spec_list, dic_names, suggestions_dic, fName_save)                        
                                else:
                                    used_dic, specify_dic = ask_create_relation(onto, csub.name, used_dic, specify_dic, restricted_sug_list, spec_list, dic_names, suggestions_dic, fName_save)                        
                                del_list, used_list, spec_list, restricted_sug_list = type_sug.restrict_suggestions(onto, csub.name, deletion_dic, used_dic, specify_dic, direction) 
                            else:
                                already_asked.append(csub)
                        else:
                            # print("Seems like no suggestions remain for %s." % csub.name)
                            already_asked.append(csub)
                    else:
                        # print("Seems like no suggestions could be found for %s." % csub.name)
                        already_asked.append(csub)
                       
                    # recursively call for all further subclasses
                    already_asked = ask_for_subclasses(onto, mc, csub, dic_tax_map, dic_dump, type_sug, fName_save, already_asked, suggestions_dic, used_dic, deletion_dic, specify_dic, direction, rel_type)
            print("<<< Returning from subclasses of %s.\n" % c.name)
    
    return already_asked

# asks the user which suggestions to not show again
# sug_dic = dictinary of suggestions
def ask_throw_out(onto, c, del_dic, sug_dic):
    import autocomplete
    from createOnto import get_superclasses
     
    # if on the highest level suggest to delete things that won't be suggested again in further steps
    if not get_superclasses(onto, onto[c].name, True):
        if query_yes_no("\nThrow out things to not show again?"):
            throws = autocomplete.suggest_stuff([x[2] for x in sug_dic[c]], "Throw out: ")
            if throws:
                del_dic[c] = []
            for t in throws.split():
                del_dic[c].append(t)
                
    return del_dic

def ask_create_relation(onto, c, used_dic, specify_dic, restricted_sug_list, spec_list, dic_names, suggestions_dic, fName_save):
    import autocomplete
    from createOnto import create_class, create_subclass_of_super
    from userRelations import check_con, check_instances_entered, create_instances, check_further_dynamic_extensions,\
    create_instance_axiom, create_subc_axiom, add_or_expression, add_rel_or_expression, generate_rel, make_disjoint, \
    add_isa_or_expression, IsA_map, IsA_inverted_map
    from TMPrel import return_TMPrel
    
    suggestions = restricted_sug_list + spec_list
    triples_list = suggestions_dic[c]
    
    if query_yes_no("\nGenerate a (new) axiom?"):
        while True:
            try:
                rel = autocomplete.suggest_relation(onto)
                if rel == "exit":
                    raise SystemExit
                # case I: more than one relation was entered
                elif len(rel.split()) > 1:
                    if any(r.endswith("-") for r in rel.split()):
                        print("Inverted Axioms for multiple relations are not possible yet. Continuing.")
                    # list of lists that stores relname and associated concepts
                    rel_or_list = []
                    for r in rel.split():
                        # relation entered already exists and is not TMPRelated or IsA
                        if onto[r] in list(onto.object_properties()) and r != "TMPRelated" and r != "IsA":
                            possible_inst_list = []
                            possible_subc_list = []
                            for s in onto[r].range:
                                if s != owl["Thing"]:
                                    if list(s.instances()):
                                        possible_inst_list.extend(i.name for i in s.instances())
                                    if s.subclasses():
                                        possible_subc_list.extend(i.name for i in s.subclasses())
                            # instances were found
                            if possible_inst_list or possible_subc_list:
                                con = autocomplete.suggest_options(c, r, possible_inst_list + possible_subc_list + [s.name for s in onto[r].range if s != owl["Thing"]])
                            # already occuring relation was entered but range had no instances
                            else:
                                con = autocomplete.suggest_options(c, r, suggestions + list(return_TMPrel(onto)) + [i.name for i in list(onto.individuals())])
                        # complete new relation name was entered
                        else:                     
                            con = autocomplete.suggest_options(c, r, suggestions + list(return_TMPrel(onto)) + [i.name for i in list(onto.individuals())])
                        
                        # case 1 & case 2: create relation and class(es) or axioms for existing instanc(es)
                        # if no () or : occur
                        if check_con(con):
                            # one or more already existing instances were entered
                            if check_instances_entered(onto, con):
                                rel_or_list.append([r, [c for c in con.split()]])
                            
                            # new or existing concepts were entered
                            else:
                                # more than one concept entered
                                if len(con.split()) > 1:
                                    for x in con.split():
                                        create_class(onto, x)
                                        if c not in specify_dic:
                                            specify_dic[c] = []
                                        specify_dic[c].append(x)
                                    rel_or_list.append([r, [x for x in con.split()]])
                                # single concept entered
                                else:
                                    create_class(onto, con)
                                    if c not in used_dic:
                                        used_dic[c] = []
                                    used_dic[c].append(con)
                                    rel_or_list.append([r, [con]])
        
                        # if con is only one concept and : or () occur
                        if not " " in con and not check_con(con):     
                            # case 3: create relation and instances of a class
                            if con.endswith("()"):
                                # remove brackets if instances should be created
                                con = con[:-2]
                                ins = autocomplete.suggest_instances(con, suggestions + list(return_TMPrel(onto)) + [i.name for i in list(onto.individuals())])
                                ins = check_further_dynamic_extensions(ins)
                                create_class(onto, con)
                                create_instances(onto, con, ins)
                                rel_or_list.append([r, [con]])   
                                
                            # case 4: create relation and add subclasses of a class
                            if con.endswith(":"):
                                # remove :
                                con = con[:-1]
                                create_class(onto, con)
                                rel_or_list.append([r, [con]])
                                
                                sub = autocomplete.suggest_subclasses(con, suggestions)
                                sub = check_further_dynamic_extensions(sub)
                                if sub:
                                    for s in sub.split():
                                        if c not in specify_dic:
                                            specify_dic[c] = []
                                        specify_dic[c].append(s)
                                        create_subclass_of_super(onto, s, con)
                                # make subclasses disjoint
                                make_disjoint(onto, con, [s for s in sub.split()])
                                
                        # if con consists of more concepts and : or () occur
                        if " " in con and not check_con(con):
                            for x in con.split():
                                if c not in specify_dic:
                                    specify_dic[c] = []
                                specify_dic[c].append(x)
                                # case 3: create relation and instances of a class
                                if x.endswith("()"):
                                    # remove brackets if instances should be created
                                    x = x[:-2]
                                    ins = autocomplete.suggest_instances(x, suggestions + list(return_TMPrel(onto)) + [i.name for i in list(onto.individuals())])
                                    ins = check_further_dynamic_extensions(ins)
                                    create_class(onto, x)
                                    create_instances(onto, x, ins)
                                                                
                                if x.endswith(":"):
                                    # remove :
                                    x = x[:-1]
                                    sub = autocomplete.suggest_subclasses(x, suggestions)
                                    sub = check_further_dynamic_extensions(sub)
                                    if sub:
                                        for s in sub.split():
                                            if c not in specify_dic:
                                                specify_dic[c] = []
                                            specify_dic[c].append(s)
                                            create_subclass_of_super(onto, s, x)
                                    # make subclasses disjoint
                                    make_disjoint(onto, x, [s for s in sub.split()])
                                    
                                # if x has no () or :
                                else:
                                    create_class(onto, x)
                            
                            # remove () and :
                            con = con.replace("()", " ")
                            con = con.replace(":", " ")
                            
                            rel_or_list.append([r, [x for x in con.split()]])
        
                        # exit
                        if con == "exit":
                            raise SystemExit
                        if not con:
                            rel_or_list = []
                    
                    # create the axiom
                    add_rel_or_expression(onto, c, rel_or_list)
                        
                # case II: single relation was entered
                elif rel:
                    # add simple "inverse" axiom
                    if rel.endswith("-"):
                        # remove :
                        rel = rel[:-1]
                        con = autocomplete.suggest_options(c, rel, suggestions + list(return_TMPrel(onto)) + [i.name for i in list(onto.individuals())], True)
                        # more than one concepts, generate multiple axioms
                        if len(con.split()) > 1:
                            for x in con.split():
                                create_class(onto, x)
                                if c not in specify_dic:
                                    specify_dic[c] = []
                                specify_dic[c].append(x)
                                # SubClassOf
                                if rel == "IsA":
                                    if onto[con] in list(onto.individuals()):
                                        create_instances(onto, c, con)
                                    else:
                                        IsA_inverted_map(onto, x, c, dic_names, triples_list)
                                else:
                                    generate_rel(onto, x, rel, c)
                        else:
                            if con:
                                create_class(onto, con)
                                if c not in specify_dic:
                                    specify_dic[c] = []
                                specify_dic[c].append(con)
                                # SubClassOf
                                if rel == "IsA":
                                    if onto[con] in list(onto.individuals()):
                                        create_instances(onto, c, con)
                                    else:
                                        IsA_inverted_map(onto, con, c, dic_names, triples_list)
                                else:
                                    generate_rel(onto, con, rel, c)
                        
                        if con == "exit":
                            raise SystemExit
                            
                    # all other types of axiom
                    else:
                        # relation entered already exists and is not TMPRelated
                        if onto[rel] in list(onto.object_properties()) and rel != "TMPRelated" and rel != "IsA":
                            possible_inst_list = []
                            possible_subc_list = []
                            for r in onto[rel].range:
                                if r != owl["Thing"]:
                                    if list(r.instances()):
                                        possible_inst_list.extend(i.name for i in r.instances())
                                    if r.subclasses():
                                        possible_subc_list.extend(i.name for i in r.subclasses())
                            # instances were found
                            if possible_inst_list or possible_subc_list:
                                con = autocomplete.suggest_options(c, rel, possible_inst_list + possible_subc_list + [r.name for r in onto[rel].range if r != owl["Thing"]])
                            # already occuring relation was entered but range had no instances
                            else:
                                con = autocomplete.suggest_options(c, rel, suggestions + list(return_TMPrel(onto)) + [i.name for i in list(onto.individuals())])
                        # complete new relation name was entered
                        else:                     
                            con = autocomplete.suggest_options(c, rel, suggestions + list(return_TMPrel(onto)) + [i.name for i in list(onto.individuals())])
                        
                        # case 1 & case 2: create relation and class(es) or axioms for existing instanc(es)
                        # if no () or : occur
                        if check_con(con):
                            # one or more already existing instances were entered
                            if check_instances_entered(onto, con):
                                create_instance_axiom(onto, c, rel, None, con)
                            
                            # new or existing concepts were entered
                            else:
                                # more than one concept entered
                                if len(con.split()) > 1:
                                    for x in con.split():
                                        create_class(onto, x)
                                        if c not in specify_dic:
                                            specify_dic[c] = []
                                        specify_dic[c].append(x)
                                        
                                    # SubClassOf
                                    if rel == "IsA":
                                        add_isa_or_expression(onto, c, rel, con)                                       
                                    else:
                                        add_or_expression(onto,c, rel, con)
                                # single concept entered
                                else:
                                    if c not in used_dic:
                                        used_dic[c] = []
                                    used_dic[c].append(con)
                                    # SubClassOf
                                    if rel == "IsA":
                                        IsA_map(onto, c, con, dic_names, triples_list)
                                    else:
                                        generate_rel(onto, c, rel, con)
        
                        # if con is only one concept and : or () occur
                        if not " " in con and not check_con(con):     
                            # case 3: create relation and instances of a class
                            if con.endswith("()"):
                                # remove brackets if instances should be created
                                con = con[:-2]
                                ins = autocomplete.suggest_instances(con, suggestions + [i.name for i in list(onto.individuals())])
                                ins = check_further_dynamic_extensions(ins)
                                create_class(onto, con)
                                create_instances(onto, con, ins)
                                
                                #create axiom
                                if rel != "TMPRelated" and rel != "IsA": 
                                    if query_yes_no("Create an existential axiom for " + c + "?" ):
                                        instance = autocomplete.suggest_options(c, rel, [i.name for i in list(onto[con].instances())])
                                        if instance =="exit":
                                            raise SystemExit
                                        create_instance_axiom(onto, c, rel, con, instance)
                                    else:
                                        # dynamically create relation with range set
                                        NewClass = types.new_class(rel, (Thing >> onto[con],))
                                elif rel == "IsA":
                                    IsA_map(onto, c, con, dic_names, triples_list)   
                                    
                            # case 4: create relation and add subclasses of a class
                            if con.endswith(":"):
                                # remove :
                                con = con[:-1]
                                create_class(onto, con)
                                #generate_rel(onto, c, rel, con)
                                sub = autocomplete.suggest_subclasses(con, suggestions + list(return_TMPrel(onto)))
                                sub = check_further_dynamic_extensions(sub)
                                if sub:
                                    for s in sub.split():                        
                                        create_subclass_of_super(onto, s, con)
                                # make subclasses disjoint
                                make_disjoint(onto, con, [s for s in sub.split()])
                                
                                #create axiom
                                if rel != "TMPRelated" and rel != "IsA": 
                                    if query_yes_no("Create an existential axiom for " + c + "?" ):
                                        subc = autocomplete.suggest_options(c, rel, [i.name for i in list(onto[con].subclasses())] + [con])
                                        if subc =="exit":
                                            raise SystemExit
                                        create_subc_axiom(onto, c, rel, con, subc)
                                        if c not in used_dic:
                                            used_dic[c] = []
                                        used_dic[c].append(con)
                                    else:
                                        # dynamically create relation with range set
                                        NewClass = types.new_class(rel, (Thing >> onto[con],))
                                        
                                elif rel == "IsA":
                                    IsA_map(onto, c, con, dic_names, triples_list)
                                        
                        # if con consists of more concepts and : or () occur
                        if " " in con and not check_con(con):
                            for x in con.split():
                                # case 3: create relation and instances of a class
                                if x.endswith("()"):
                                    # remove brackets if instances should be created
                                    x = x[:-2]
                                    ins = autocomplete.suggest_instances(x, suggestions + list(return_TMPrel(onto)) + [i.name for i in list(onto.individuals())])
                                    ins = check_further_dynamic_extensions(ins)
                                    create_class(onto, x)
                                    create_instances(onto, x, ins)
                                                                
                                if x.endswith(":"):
                                    # remove :
                                    x = x[:-1]
                                    sub = autocomplete.suggest_subclasses(x, suggestions)
                                    sub = check_further_dynamic_extensions(sub)
                                    if sub:
                                        for s in sub.split():
                                            if c not in specify_dic:
                                                specify_dic[c] = []
                                            specify_dic[c].append(s)
                                            create_subclass_of_super(onto, s, x)
                                    # make subclasses disjoint
                                    make_disjoint(onto, x, [s for s in sub.split()])
                                # if x has no () or :
                                else:
                                    create_class(onto, x)
                            
                            # remove () and :
                            con = con.replace("()", " ")
                            con = con.replace(":", " ")
                            
                            # SubClassOf
                            if rel == "IsA":
                                add_isa_or_expression(onto, c, rel, con)
                            else:
                                add_or_expression(onto, c, rel, con)
        
                    # exit
                    if con == "exit":
                        raise SystemExit
                    if not con:
                        continue
                    
                # nothing entered    
                if not rel:
                    return used_dic, specify_dic
                # no concept entered
                if not con:
                    continue
                
                file_modification.save_in_file(onto, fName_save)
                file_modification.save_dic_in_file(dic_names, settings.fName_dic_tax_map)

            except ValueError as e:
                print("Sorry, I didn't understand that.")  

    return used_dic, specify_dic                      

## returns the CN id for a suggestion from the suggestions dic
#def get_CNid(onto, oclass, suggestions_dic):
#    import request, re
#    triples_list = []
#    
#    for k in suggestions_dic.keys():
#        triples_list.extend(suggestions_dic[k])
#    
#    for e in triples_list:
#        if oclass.name == e[2]:
#            return e[0]
#        else:
#            continue
#        
#    print("There was no respective entity for %s. We are trying to find the most suitable." % oclass.name)
#    #==============================================================================
#    # Could implement a function here that tries to find an appropriate CN id for
#    # any concept name entered!             
#    #==============================================================================
#    n = oclass.name
#    incl_words = re.findall('[A-Z][^A-Z]*', n)
#    n_search = '+'.join(incl_words)
#    r = request.CNRequest(None, 5, None, None, n_search)
#    obj = r.get_obj()
#    try:
#        r_test = request.CNRequest(obj['@id'], 1, None, 1, None)
#        obj_test = r_test.get_obj()
#        try: 
#            obj_test['error']
#            print("No entity was found.")
#            return None
#        except KeyError:
#            if query_yes_no("Store %s as respective CN id?" % obj['@id']):
#                return obj['@id']
#            else:
#                return None
#    except KeyError:
#        print("No entity was found.")
#        return None

def print_suggestions_right(c, restricted_sug_list, spec_list, rel_type):
    if restricted_sug_list:
        print("\nThese are possible %s of %s\n" % (rel_type, c))
        print("-------------------------------------------")
        print(sorted(restricted_sug_list))
        print("-------------------------------------------")
    if spec_list:
        print("\nThese %s were used and could be suitable for specification:\n" % rel_type)
        print(set(sorted(spec_list)))
        print("-------------------------------------------")
    elif (not restricted_sug_list and not spec_list):
        print("\nSeems like there are no more suggestions for %s of %s." % (rel_type, c))
        print("You can can still add an axiom below.")
        
        
def print_suggestions_left(c, restricted_sug_list, spec_list, rel_type):
    if restricted_sug_list:
        print("\nThese are possible things with %s %s\n" % (rel_type, c))
        print("-------------------------------------------")
        print(sorted(restricted_sug_list))
        print("-------------------------------------------")
    if spec_list:
        print("\nThese things were already used as %s and could be suitable for specification:\n" % rel_type)
        print(set(sorted(spec_list)))
        print("-------------------------------------------")
    elif (not restricted_sug_list and not spec_list):
        print("\nSeems like there are no more suggestions for things with %s %s." % (rel_type, c))
        print("You can can still add an axiom below.")

            
                                