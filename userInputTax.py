# -*- coding: utf-8 -*-
import createOnto, userInput

def del_dump_move(tax, dic_names, sub): # is sub is True, then sublist, else superlist
    selectall = ("all","a","ALL","A")
    inverse = ("-i", "-I", "not", "NOT") 
    
#==============================================================================
#  Deleting   
#==============================================================================
    while True:
        try:
            print("Which concepts do you want to delete? ", end='')
            
            selection_del = input()
            # exit
            if selection_del == "exit":
                raise SystemExit
            # hit enter to continue           
            if not selection_del:
                del_list = []
            # select all
            elif selection_del in selectall: 
                if sub:
                    del_list = list(range(0, len(tax.subl)))
                else:
                    del_list = list(range(0, len(tax.supl)))
            # choices entered
            else:
                 # inverse choices
                 if selection_del.startswith(inverse):
                    remove_start = selection_del.split(" ", 1)
                    remove_list = [int(x) for x in remove_start[1].split()]
                    if sub:
                        for n in remove_list:
                            if not n in list(range(0, len(tax.subl))):
                                print("Some index was not there.")
                        del_list = [x for x in list(range(0, len(tax.subl))) if x not in remove_list]

                    else:
                        for n in remove_list:
                            if not n in list(range(0, len(tax.supl))):
                                print("Some index was not there.")
                        del_list = [x for x in list(range(0, len(tax.supl))) if x not in remove_list]

                 else:
                    del_list = [int(x) for x in selection_del.split()]
    
        except ValueError:
            print("Sorry, I didn't understand that.")
            #return to the start of the loop
            continue
        except IndexError:
            print("Index not valid. Try again.")
            #return to the start of the loop
            continue
        else:
            #ready to exit the loop.
            break    
#==============================================================================
#  Moving to Dump for later
#==============================================================================
    while True:
        try:
            print("Which concepts do you want to keep for later? ", end='')
        
            selection_dump = input()  
            # exit
            if selection_dump == "exit":
                raise SystemExit
            # hit enter to continue
            if not selection_dump:
                dump_list = []
            # select all       
            elif selection_dump in selectall:
                if sub:
                    dump_list = list(range(0, len(tax.subl)))
                else:
                    del_list = list(range(0, len(tax.supl)))
            # choices entered            
            else:
                if sub:
                    # inverse choices
                    if selection_dump.startswith(inverse):
                        remove_start = selection_dump.split(" ", 1)
                        remove_list = [int(x) for x in remove_start[1].split()]
                        for n in remove_list:
                            if not n in list(range(0, len(tax.subl))):
                                print("Some index was not there.")
                        dump_list = [x for x in list(range(0, len(tax.subl))) if x not in remove_list]
                    else:
                        dump_list = [int(x) for x in selection_dump.split()]
                        
                    tax.dump.extend([tax.subl[x] for x in dump_list])
                else: 
                    # inverse choices
                    if selection_dump.startswith(inverse):
                        remove_start = selection_dump.split(" ", 1)
                        remove_list = [int(x) for x in remove_start[1].split()]
                        for n in remove_list:
                            if not n in list(range(0, len(tax.supl))):
                                print("Some index was not there.")
                        dump_list = [x for x in list(range(0, len(tax.supl))) if x not in remove_list]
                    else:
                        dump_list = [int(x) for x in selection_dump.split()]
                        
                    tax.dump.extend([tax.supl[x] for x in dump_list])
        except ValueError:
            print("Sorry, I didn't understand that.")
            #return to the start of the loop
            continue
        except IndexError:
            print("Index not valid. Try again.")
            #return to the start of the loop
            continue        
        else:
            #ready to exit the loop.
            break    
#==============================================================================
# Make Sub to Super and Super to Sub    
#==============================================================================
    while True:
        try:
            print("Which concepts do you want to move to %s? " % ("Super" if sub else "Sub"), end='') 
        
            selection_move = input()    
            # exit
            if selection_move == "exit":
                raise SystemExit
            # hit enter to continue
            if not selection_move:
                move_list = []
            # select all                       
            elif selection_move in selectall:
                if sub:
                    move_list = list(range(0, len(tax.subl)))
                else:
                    move_list = list(range(0, len(tax.supl)))
            # choices entered            
            else:
                move_list = [int(x) for x in selection_move.split()]
                
                if sub:
                    for x in move_list:
                        #case sub to super
                        #as subclass of some existing class
                        if userInput.query_yes_no("Make %s a subclass of some (existing) class(es)?" % tax.subl[x][2]):
                            print(createOnto.get_superclasses(tax.onto, tax.oclass)) 
                            sup = input("Choose one or more of the above or enter a new Classname: ")
                            #no input (if chosen by mistake nothing is created)                            
                            if not sup:
                                continue
                            else:
                                for c in sup.split():
                                    createOnto.create_subclass_of_super(tax.onto, tax.subl[x][2], c)
                                    dic_names[tax.subl[x][2]] = tax.subl[x][0]
                        #as general superclass
                        else: 
                            createOnto.create_class(tax.onto, tax.subl[x][2])
                        
                        createOnto.add_superclass_of_sub(tax.onto, tax.oclass, tax.subl[x][2])            
                        #print("[[[making %s a superclass of %s]]]" % (tax.subl[x][2],tax.oclass))
                else:
                   for x in move_list:
                        #case super to sub
                        createOnto.create_superclass_of_sub(tax.onto, tax.supl[x][2], tax.oclass)
                        dic_names[tax.supl[x][2]] = tax.supl[x][0]
                        #print("[[[making %s a subclass of %s]]]" % (tax.supl[x][2],tax.oclass))
                        
                        #as superclass of some existing classes
                        if userInput.query_yes_no("\nMake %s a superclass of some existing class(es)?" % tax.supl[x][2]):
                            print(createOnto.get_subclasses(tax.onto, tax.oclass)) 
                            sub = input("Choose the subclass(es): ")
                            #no input (if chosen by mistake nothing is created)                            
                            if not sub:
                                continue
                            for c in sub.split():
                                createOnto.add_superclass_of_sub(tax.onto, c, tax.supl[x][2])
                            
                        
                     
                   
        except ValueError:
            print("Sorry, I didn't understand that.")
            #return to the start of the loop
            continue
        except IndexError:
            print("Index not valid. Try again.")
            #return to the start of the loop
            continue
        else:
            #ready to exit the loop.
            break     
           
    # delete everything that was changed from sublist or superlist
    complete_list = list(set(del_list+dump_list+move_list))
    complete_list = sorted(complete_list, key=int, reverse=True)
    
    for obj in complete_list:
        try:
            if sub:
                del tax.subl[obj]
            else:
                del tax.supl[obj]
        except IndexError:
            print("Some index was not valid or you entered something twice. What caused the problem will be ignored.") 


def sub_or_super(options_list, oclass, choice):
    selectall = ("all","a","ALL","A")
    inverse = ("-i", "-I", "not", "NOT")

    while True:
        try:
            if choice == "sup":
                print("Choose Superclasses of %s: " % oclass, end='')
            elif choice == "sub":
                print("Choose Subclasses of %s: " % oclass, end='')
            else:
                print("Which concepts do you want to keep for later? ", end='')
            
            selection = input()
            # exit
            if selection == "exit":
                raise SystemExit
            # hit enter to continue           
            if not selection:
                break
            # select all
            elif selection in selectall: 
                selection =  list(range(0, len(options_list)))
            else:
                if selection.startswith(inverse):
                        remove_start = selection.split(" ", 1)
                        remove_list = [int(x) for x in remove_start[1].split()]
                        for n in remove_list:
                            if not n in list(range(0, len(options_list))):
                                print("Some index was not there.")
                        selection = [x for x in list(range(0, len(options_list))) if x not in remove_list]
                else:
                    selection = [int(x) for x in selection.split()]
            
            try: 
                selection = [options_list[i] for i in selection]
            
            except IndexError as e:
                print("You chose a number not present.")
                #return to the start of the loop
                continue    
    
        except ValueError:
            print("Sorry, I didn't understand that.")
            #return to the start of the loop
            continue
        else:
            #ready to exit the loop.
            break
    
    return selection     
    
def choose_move(some_list):
    while True:
        firstsecond = input("Enter the numbers of the concepts you want to link: ")
        
        if firstsecond == "":
                return "",""       
        else:
            try:          
                first, second = firstsecond.split()
                
                if first == "exit" or second == "exit":
                    raise SystemExit
                    
                else:
                    if userInput.query_yes_no("Make %s subclass of %s?" %(some_list[int(first)].name, some_list[int(second)].name)):
                        return some_list[int(first)], some_list[int(second)]
                    elif userInput.query_yes_no("Make %s subclass of %s?" %(some_list [int(second)].name, some_list[int(first)].name)):
                        return some_list [int(second)], some_list[int(first)]
            
            except ValueError as e:
                    print("Something you entered was not valid. Try again.")
        
        
   
