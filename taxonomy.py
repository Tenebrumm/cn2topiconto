# -*- coding: utf-8 -*-
import request, userInputTax, userInput, ontoParsing, settings
import re, itertools
from operator import itemgetter

class TaxSubSup():
    language = 'en'    
    
    def __init__(self, onto, cdef):
        self.onto = onto
        
        self.cdef = cdef
        self.cid = cdef[0]
        self.c = cdef[1]
        self.oclass = cdef[2]
        
        self.subl = [] # list of subclasses [id, label, onto]
        self.supl = [] # list of superclasses [id,label, onto]
        self.dump = [] # dump for unused classes
    
    def add_subclasses(self, noun = True):
        # requests
        r = request.CNRequest(self.cid, 4, "IsA")
        obj = r.get_obj()
       
        self.add(obj, noun, r.get_weight())
#        print(r.get_weight())
    
    def add_superclasses(self, noun = True):
        # requests
        r = request.CNRequest(self.cid, 3, "IsA")
        obj = r.get_obj()
    
        self.add(obj, noun, r.get_weight())
#        print(r.get_weight())
    
    def add(self, obj, noun, weight):
        # adds the CN results to subl
        subl = []
#        weightlist1 = []
#        weightlist2 = []
        if not self.subl:
            for e in (obj['edges']):
                try:
                    # language has to match
                    # check if not in the list already
    
                    if e['start']['language'] == self.language and e['end']['language'] == self.language and not e['start']['@id'] in [sub[0] for sub in self.subl]: 
                        # subconcept -> concept
                        x = [e['start']['@id'], e['start']['label'], None]
                        if e['end']['@id'] == self.cid and e['weight'] >= weight and x not in self.subl:
                            self.subl.append(x)
#                            weightlist1.append((e['start']['label'], e['weight']))
                        if e['end']['@id'] == self.cid+"/n" and e['weight'] >= weight and x not in (subl or self.subl):
                            subl.append(x)
#                            weightlist2.append((e['start']['label'], e['weight']))
                            
                except KeyError:
                    continue
                
            # if noun is true or not enough results were found add concepts with tag
            if noun or (not noun and len(self.subl)<5):
                self.subl.extend(subl)
#                weightlist1.append(weightlist2)
#            print(weightlist1)
        
        # adds the CN results to supl
        supl = []
        if not self.supl:
            for e in (obj['edges']):
                try:
                    # language has to match
                    # check if not in the list already
                    if e['start']['language'] == self.language and e['end']['language'] == self.language and not e['start']['@id'] in [sup[0] for sup in self.supl]: 
                        # concept -> superconcept
                        x = [e['end']['@id'], e['end']['label'], None]
                        if e['start']['@id'] == self.cid and e['weight'] >= weight and x not in self.supl:
                            self.supl.append(x)
                        if e['start']['@id'] == self.cid+"/n" and e['weight'] >= weight and x not in (supl or self.supl):
                            supl.append(x)
                            
                except KeyError:
                    continue
                
            # if noun is true or not enough results were found add concepts with tag
            if noun or (not noun and len(self.supl)<5):
                self.supl.extend(supl)
        
       
    def add_related_to(self):
        # requests
        r = request.CNRequest(self.cid, 2, "RelatedTo")
        obj = r.get_obj()
#        print(r1.get_weight())
        
        options_list = []
        # adds the CN results to subl
        for e in (obj['edges']):
            try:
                # language has to match
                # check if not in the list already
                if e['start']['language'] == self.language and e['end']['language'] == self.language and not e['start']['@id'] in [sub[0] for sub in self.subl]: 
                    # check which ending is concept                    
                    if e['end']['@id'] == (self.cid or self.cid+"/n"):                    
                        x = [e['start']['@id'], e['start']['label'], None]
                    else: 
                        x = [e['end']['@id'], e['end']['label'], None]

                    if e['weight'] >= r.get_weight() and x not in self.subl and x not in self.supl and x not in options_list:
                           options_list.append(x)
                   
            except KeyError:
                continue
        
        if options_list:
            print("\nThose might be sub- or superclasses too: ")
            self.add_name_oclasses(*options_list)
            options_list = sorted(options_list, key=itemgetter(2))
            userInput.print_list_with_index(options_list)
            # subclasses
            selection = userInputTax.sub_or_super(options_list, self.oclass, "sub")
            self.subl.extend(selection)
            # superclasses
            selection = userInputTax.sub_or_super(options_list, self.oclass, "sup")
            self.supl.extend(selection)
            # dump
            selection = userInputTax.sub_or_super(options_list, self.oclass, "")
            self.dump.extend(selection)
            
            # reorder complete list by alphabet again
            self.reorder_list_by_oclass()
            
    def add_name_oclasses(self, *related_list):
        #parsing and setting of classnames for relatedlist       
        if related_list:
            for i in related_list:
                new = ontoParsing.remove_articles(' '+i[1]+' ') #removes articles
                new = new.title() #capitalizes words
                new = ontoParsing.remove_spaces(new) #removes all spaces
                i[2] = new  
        #parsing and setting of classnames in subl and supl
        for i in itertools.chain(self.subl, self.supl):
            new = ontoParsing.remove_articles(' '+i[1]+' ') #removes articles
            new = new.title() #capitalizes words
            new = ontoParsing.remove_spaces(new) #removes all spaces
            i[2] = new
 
            
        self.reorder_list_by_oclass()      
    
    def verify(self, dic_names):
        from related_check import related_check

        # removes the main concept from all sub and super lists
        # this makes it not show up for verification as a superclass for subclasses again
        for e in self.subl:
            if e[2] == settings.nameoclass:
                self.subl.remove(e)
        for e in self.supl:
            if e[2] == settings.nameoclass:
                self.supl.remove(e) 
        #subclasses
        # the subclass list is empty
        if not self.subl:
            pass
            
        else:
            print("Possible subclasses of " + self.oclass)
            userInput.print_list_with_index(self.subl)
            # related check
            if settings.numberbatch:
                self.subl = related_check(settings.namecid, self.subl)                       
                userInput.print_list_with_index(self.subl)
            userInputTax.del_dump_move(self, dic_names, True) # true for sublist
        
        #superclasses
        # the superclass list is empty
        if not self.supl:
            pass
            
        else:
            print("\nPossible superclasses of " + self.oclass)
            userInput.print_list_with_index(self.supl)
            # related check
            if settings.numberbatch:
                self.supl = related_check(settings.namecid, self.supl)                       
                userInput.print_list_with_index(self.supl)
            userInputTax.del_dump_move(self, dic_names, False) # false for superlist


    def get_element_from_subl(self, oclass):
        for s in self.subl:
            if s[2] == oclass:
                return s
    
    def print_subclasses(self):
        print(self.subl)        
       
    def reorder_list_by_oclass(self):
        self.subl = sorted(self.subl, key=itemgetter(2))
        self.supl = sorted(self.supl, key=itemgetter(2))          
    

    