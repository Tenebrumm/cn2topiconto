# -*- coding: utf-8 -*-

# -*- coding: utf-8 -*-

from owlready2 import *
import ontoParsing
import createCNstorage_Capable, prepareSuggestions
import genericRelationsInput
from userInput import query_yes_no

def run_capable(onto, dic_tax_map, dic_dump, fName_save):    
    cap = createCNstorage_Capable.CNstorage_Capable(onto, ["CapableOf", "ReceivesAction"], "capabilities", False) # HERE: change to False
    # loc.print_CNdics()
    cap_sug = prepareSuggestions.Suggestions(onto, cap, "capability")
    #loc_sug.print_suggestions()
    
    print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
    if query_yes_no("Do you want suggestions of the form \"C CapableOf x\"? (capabilities of C)"):
        genericRelationsInput.suggest_type(onto, dic_tax_map, dic_dump, cap_sug, fName_save, "right", "capabilities")
    print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
    if query_yes_no("Do you want suggestions of the form \"x CapableOf C\"? (only useful if your ontology contains action concepts)"):
        genericRelationsInput.suggest_type(onto, dic_tax_map, dic_dump, cap_sug, fName_save, "left", "capability")
    else:
        print("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<")

#def suggest_capable_left(onto, dic_tax_map, dic_dump, cap_sug, fName_save):
#    import file_modification
#    
#    print("\n--------------------- Part 1: Capabilities ---------------------")
#    print("\nSuggested relations: Can, HasState, HasProperty\n")
#    
#    # storage for all concepts that should be deleted for future suggestions
#    del_dic = {}
#    # storages for all concepts assigned higher up already 
#    # only stores keys for concepts that had something added    
#    used_dic = {}
#    spec_dic = {}
#
#    # initialize queue
#    que = genericRelations.ClassQueue()
#    que.update_class_queue(onto, dic_tax_map) 
#    
#    # run while still elements in queue
#    while que.open_list:
#        c = que.open_list.popleft()
#        
#        del_list, used_list, spec_list, rs_left = cap_sug.restrict_suggestions(onto, c, del_dic, used_dic, spec_dic, "left") 
#                
#        if c in cap_sug.suggestions_dic_left:
#            if rs_left:
#                print("\nThese are possible capabilities of %s\n" % c)
#                print(sorted(rs_left))
#            else:
#                print("\nSeems like no specific suggestions for capabilities of %s were found.\n" %c)
#            
#            if spec_list:
#                print("\nThese capabilities were used and could be suitable for specification:\n")
#                print(sorted(spec_list))
#            
#            # ask and update del_dic
#            del_dic = genericRelations.ask_throw_out(onto, c, del_dic, cap_sug.suggestions_dic_left)
#            
#            del_list, used_list, spec_list, rs_left = cap_sug.restrict_suggestions(onto, c, del_dic, used_dic, spec_dic, "left") 
#
#            # ask for relations to create and update spec_dic and used_dic
#            used_dic, spec_dic = genericRelations.ask_create_relation(onto, c, used_dic, spec_dic, rs_left)
#
#        # save        
#        file_modification.save_in_file(onto, fName_save)
#        
#        # exit condition, list is empty again
#        if not que.open_list:
#            return
#        else:
#             # update queue
#            que.update_class_queue(onto, dic_tax_map, c)
#            
#def suggest_capable_right(onto, dic_tax_map, dic_dump, cap_sug, fName_save):
#    import file_modification
#    
#    print("\n-------- Part 2: Things that might be located within ---------")
#    print("\nSuggested relations: ContainedIn, MadeOf, UsuallyFoundIn, \n")
#    
#    # storage for all concepts that should be deleted for future suggestions
#    del_dic = {}
#    # storages for all concepts assigned higher up already 
#    # only stores keys for concepts that had something added    
#    used_dic = {}
#    spec_dic = {}
#
#    # initialize queue
#    que = genericRelations.ClassQueue()
#    que.update_class_queue(onto, dic_tax_map) 
#    
#    # run while still elements in queue
#    while que.open_list:
#        c = que.open_list.popleft()
#        
#        del_list, used_list, spec_list, rs_right = cap_sug.restrict_suggestions(onto, c, del_dic, used_dic, spec_dic, "right") 
#                
#        if c in cap_sug.suggestions_dic_left:
#            if rs_right:
#                print("\nThese are possible things with location %s\n" % c)
#                print(sorted(rs_right))
#            else:
#                print("\nSeems like no specific suggestions for things with location %s were found.\n" %c)
#            
#            if spec_list:
#                print("\nThese things were already used as locations and could be suitable for specification:\n")
#                print(sorted(spec_list))
#            
#            # ask and update del_dic
#            del_dic = genericRelations.ask_throw_out(onto, c, del_dic, cap_sug.suggestions_dic_right)
#            
#            del_list, used_list, spec_list, rs_right = cap_sug.restrict_suggestions(onto, c, del_dic, used_dic, spec_dic, "right") 
#
#            # ask for relations to create and update spec_dic and used_dic
#            used_dic, spec_dic = genericRelations.ask_create_relation(onto, c, used_dic, spec_dic, rs_right)
#
#        # save        
#        file_modification.save_in_file(onto, fName_save)
#        
#        # exit condition, list is empty again
#        if not que.open_list:
#            return
#        else:
#             # update queue
#            que.update_class_queue(onto, dic_tax_map, c)
