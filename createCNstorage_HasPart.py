# -*- coding: utf-8 -*-

# -*- coding: utf-8 -*-
import settings, file_modification
from owlready2 import *
import request, ontoParsing

# NOTE: Specifically working for HasPart

class CNstorage_HasPart:
    # rel is the CN relation, phrase the respective word for output generation
    def __init__(self, onto, rel, phrase, load):
        self.rel = rel
        self.phrase = phrase
        self.CN_dic_left = {}
        self.CN_dic_right = {}       
        
        dic_tax_map = file_modification.read_dic_from_file(settings.fName_dic_tax_map)
        
        if load:
            self.CN_dic_left = file_modification.read_dic_from_file("/home/katinka/Dokumente/Master Thesis/Onto/CN_dic_left_parts")
            self.CN_dic_right = file_modification.read_dic_from_file("/home/katinka/Dokumente/Master Thesis/Onto/CN_dic_right_parts")
        else:
            self.build_CN_dic(onto, dic_tax_map)
            file_modification.save_dic_in_file(self.CN_dic_left,"/home/katinka/Dokumente/Master Thesis/Onto/CN_dic_left_parts")
            file_modification.save_dic_in_file(self.CN_dic_right,"/home/katinka/Dokumente/Master Thesis/Onto/CN_dic_right_parts")


    def build_CN_dic(self, onto, dic_tax_map):
        from genericRelations import remove_c
        
        print("[Start building suggestions for %s]" % self.phrase)
        counter_now = 0
        counter_total = len(dic_tax_map)
        
        for c in list(onto.classes()):
            # only if key is in the dictionary (only base taxonomy)
            if c.name in dic_tax_map:
                cid = dic_tax_map[c.name]
                cid = ontoParsing.remove_nountag(cid) # remove nountag if there is one in dict
                
                print("[Getting suggestions for %s (%s/%s)]" % (c.name,counter_now+1, counter_total))
                options_list1, options_list2 = CNstorage_HasPart.get_relations(self, cid)

                ontoParsing.add_name_oclasses(options_list1)
                ontoParsing.add_name_oclasses(options_list2)
                
                # need to remove concept itself as it often occures in C PartOf C
                options_list1 = remove_c(c, options_list1)
                options_list2 = remove_c(c, options_list2)    
                
                # remove everything that already has a key in tax_map (is part of tax)
                for e in list(onto.classes()):
                    if e.name in dic_tax_map:
                        options_list1 = remove_c(e, options_list1)
                        options_list2 = remove_c(e, options_list2) 
                
                # only store if not empty
                if options_list1:
                    # parts of c (CARE: other way around to ensure correct output question)
                    self.CN_dic_right[c.name] = options_list1
                if options_list2:
                    # that has part c (CARE: other way around to ensure correct output question)
                    self.CN_dic_left[c.name] = options_list2

                counter_now += 1
                
       
    def get_relations(self, cid):
        r1 = request.CNRequest(cid, 3, "HasA") # start = cid
        obj1 = r1.get_obj()
        r2 = request.CNRequest(cid, 4, "HasA") # end = cid
        obj2 = r2.get_obj()
        r3 = request.CNRequest(cid, 3, "PartOf")
        obj3 = r3.get_obj()
        r4 = request.CNRequest(cid, 4, "PartOf")
        obj4 = r4.get_obj()
        
        r1.weight = max(r1.weight, r4.weight)
        r4.weight = max(r1.weight, r4.weight)
        r2.weight = max(r2.weight, r3.weight)
        r3.weight = max(r2.weight, r3.weight)
        
        options_list1 = [] # parts of c
        options_list2 = [] # stuff that has c as part       
            
        # adds the CN results 
        for e in (obj1['edges']):
            try:
                # language has to match
                if e['start']['language'] == settings.language and e['end']['language'] == settings.language: 
                    x1 = [e['start']['@id'], e['start']['label'], None]
                    x2 = [e['end']['@id'], e['end']['label'], None]
    
                    if e['weight'] >= r1.get_weight() and x2 not in options_list1:
                           options_list1.append(x2)
       
            except KeyError:
                continue
            
        for e in (obj4['edges']):
            try:
                # language has to match
                if e['start']['language'] == settings.language and e['end']['language'] == settings.language: 
                    x1 = [e['start']['@id'], e['start']['label'], None]
                    x2 = [e['end']['@id'], e['end']['label'], None]
    
                    if e['weight'] >= r4.get_weight() and x1 not in options_list1:
                           options_list1.append(x1)
       
            except KeyError:
                continue  
            
        for e in (obj2['edges']):
            try:
                # language has to match
                if e['start']['language'] == settings.language and e['end']['language'] == settings.language: 
                    x1 = [e['start']['@id'], e['start']['label'], None]
                    x2 = [e['end']['@id'], e['end']['label'], None]
    
                    if e['weight'] >= r2.get_weight() and x1 not in options_list2:
                           options_list2.append(x1)
                           
            except KeyError:
                continue
            
        for e in (obj3['edges']):
            try:
                # language has to match
                if e['start']['language'] == settings.language and e['end']['language'] == settings.language: 
                    x1 = [e['start']['@id'], e['start']['label'], None]
                    x2 = [e['end']['@id'], e['end']['label'], None]
    
                    if e['weight'] >= r3.get_weight() and x2 not in options_list2:
                           options_list2.append(x2)
       
            except KeyError:
                continue   
            
        return(options_list1, options_list2)
        
    def print_CNdics(self):
        for key, values in self.CN_dic_left.items():
            print(key, [x[2] for x in values])
        for key, values in self.CN_dic_right.items():
            print(key, [x[2] for x in values])