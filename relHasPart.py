# -*- coding: utf-8 -*-

from owlready2 import *
import settings, file_modification
import createCNstorage_HasPart, prepareSuggestions
import genericRelationsInput
from userInput import query_yes_no

def run_hasPart(onto, dic_tax_map, dic_dump, fName_save):    
    part = createCNstorage_HasPart.CNstorage_HasPart(onto, ["PartOf","HasA"], "parts", False) # HERE: change to False
    part_sug = prepareSuggestions.Suggestions(onto, part, "HasPart")
    
    with onto:
        # define general relation
        class HasPart(Thing >> Thing, TransitiveProperty):
            pass
        class PartOf(Thing >> Thing):
            inverse_property = HasPart

    print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
    if query_yes_no("Do you want suggestions x of the form \"A HasPart x\"? (parts of A)"):   
        genericRelationsInput.suggest_type(onto, dic_tax_map, dic_dump, part_sug, fName_save, "right", "parts")
    print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
    if query_yes_no("Do you want suggestions x of the form \"x HasPart A\"? (things with A as part)"):
        genericRelationsInput.suggest_type(onto, dic_tax_map, dic_dump, part_sug, fName_save, "left", "parts")    
    else:
        print("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<")


def ask_create_HasPart(onto, c, used_dic, specify_dic, restricted_sug_list, spec_list, direction, fName_save):
    import autocomplete
    from createOnto import create_class, create_subclass_of_super
    from userRelations import check_con, check_instances_entered, create_instances, create_instance_axiom, create_subc_axiom, add_or_expression, generate_rel, make_disjoint, check_further_dynamic_extensions

    
    suggestions = restricted_sug_list + spec_list
    
    if direction == "left":
        rel = "PartOf"
    if direction == "right":
        rel = "HasPart"
    
    if query_yes_no("\nGenerate a (new) axiom with HasPart?"):
            
        while True:
            try:
                if suggestions:
                    con = autocomplete.suggest_options(c, rel, suggestions)
                        
                # case 1 & case 2: create relation and class(es) or axioms for existing instanc(es)
                # if no () or : occur
                if check_con(con):
                    # one or more already existing instances were entered
                    if check_instances_entered(onto, con):
                        create_instance_axiom(onto, c, rel, None, con)
                    
                    # new or existing concepts were entered
                    else:
                        # more than one concept entered
                        if len(con.split()) > 1:
                            for x in con.split():
                                create_class(onto, x)
                                if c not in specify_dic:
                                    specify_dic[c] = []
                                specify_dic[c].append(x)
                            add_or_expression(onto,c, rel, con)
                        # single concept entered
                        else:
                            if c not in used_dic:
                                used_dic[c] = []
                            used_dic[c].append(con)
                            generate_rel(onto, c, rel, con)
    
                # if con is only one concept and : or () occur
                if not " " in con and not check_con(con):

                    # case 3: create relation and instances of a class
                    if con.endswith("()"):
                        # remove brackets if instances should be created
                        con = con[:-2]
                        ins = autocomplete.suggest_instances(con, suggestions)
                        ins = check_further_dynamic_extensions(ins)
                        create_class(onto, con)
                        create_instances(onto, con, ins)
                        
                        #create axiom
                        if query_yes_no("Create an existential axiom for " + c + "?" ):
                            instance = autocomplete.suggest_options(c, rel, [i.name for i in list(onto[con].instances())])
                            create_instance_axiom(onto, c, rel, None, instance)
                        
                    # case 4: create relation and add subclasses of a class
                    if con.endswith(":"):
                        # remove :
                        con = con[:-1]
                        sub = autocomplete.suggest_subclasses(con, suggestions)
                        sub = check_further_dynamic_extensions(sub)
                        if sub:
                            for s in sub.split():                        
                                create_subclass_of_super(onto, s, con)
                        # make subclasses disjoint
                        make_disjoint(onto, con, [s for s in sub.split()])
                        if query_yes_no("Create an existential axiom for " + c + "?" ):
                            subc = autocomplete.suggest_options(c, rel, [i.name for i in list(onto[con].subclasses())] + [c])
                            create_subc_axiom(onto, c, rel, con, subc, True)
                            if c not in used_dic:
                                used_dic[c] = []
                            used_dic[c].append(con)

    
                # if con consists of more concepts and : or () occur
                if " " in con and not check_con(con):
                    for x in con.split():
                        # case 3: create relation and instances of a class
                        if x.endswith("()"):
                            # remove brackets if instances should be created
                            x = x[:-2]
                            ins = autocomplete.suggest_instances(x, suggestions + [i.name for i in list(onto.individuals())])
                            ins = check_further_dynamic_extensions(ins)
                            create_class(onto, x)
                            create_instances(onto, x, ins)
                                                    
                        if x.endswith(":"):
                            # remove :
                            x = x[:-1]
                            sub = autocomplete.suggest_subclasses(x, suggestions)
                            sub = check_further_dynamic_extensions(sub)
                            if sub:
                                for s in sub.split():
                                    if c not in specify_dic:
                                        specify_dic[c] = []
                                    specify_dic[c].append(s)
                                    create_subclass_of_super(onto, s, x)
                            # make subclasses disjoint
                            make_disjoint(onto, x, [s for s in sub.split()])                                  
                        # if x has no () or :
                        else:
                            create_class(onto,x)
                            
                    # remove () and :
                    con = con.replace("()", " ")
                    con = con.replace(":", " ")
                    
                    add_or_expression(onto, c, rel, con)
    
                # exit
                if con == "exit":
                    raise SystemExit
                # nothing entered    
                if not con:
                    break
            
                file_modification.save_in_file(onto, fName_save)
                    
            except ValueError as e:
                print("Sorry, I didn't understand that.")  
    
    if direction == "right":
        if query_yes_no("Do you want to create a general class %sParts and a forall axiom?" %c):
            # create .Parts, add forall axiom
            with onto:        
                new = "%sPart" % c
                # create a class .Parts and make it subclass of Part
                create_subclass_of_super(onto, new, "Part") 
                # add . forAll has_part .Parts
                onto[c].is_a.append(onto.HasPart.only(onto[new])) 
                
                # add as subclasses for APart
                parts_list = []
                for x in onto[c].is_a:
                    if type(x) == Restriction and x.property == onto.HasPart:
                        print(x.value)
                        if type(x.value) == class_construct.Or:
                            parts_list.extend([p.name for p in x.value.Classes])
                        else:
                            parts_list.append(x.value.name)
                parts_list=set(parts_list)
                parts_list.remove(new)
                for p in parts_list:
                    create_subclass_of_super(onto, p, new)
                            
           
    file_modification.save_in_file(onto, fName_save)
    return used_dic, specify_dic                          