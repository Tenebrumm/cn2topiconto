# -*- coding: utf-8 -*-
import requests, settings

class CNRequest:
    host = 'http://api.conceptnet.io/'
    language = 'c/%s/'% settings.language
    
    # req_type 0: related (c is filter concept), 1: concept, 2: query with a specific relation, 
    # 3: start with specific relation, 4: end with specific relation, 5: uri request
    def __init__(self, cid, req_type, rel = None, weight = None, c = None):
        self.t = req_type    
        self.cid = cid
        self.r = rel
        self.c = c
        self.obj = self.set_obj()
        
        # only has weight if not case 0 or case 5 
        if c is None:
            if weight:
                self.weight = weight
            else:
                self.weight = self.calculate_weight()
 
    
    def set_obj(self):
        if self.t == 0:
            payload = {'filter': '%s' % self.c, 'limit': '10000'}
            return requests.get('%s%s%s' % (self.host, 'related', self.cid), params=payload).json()
        if self.t == 1:
            payload = {'filter': '/c/en', 'limit': '10000'}
            return requests.get('%s%s' % (self.host, self.cid), params=payload).json()
        if self.t == 2:
            payload = {'node':'%s' % self.cid, 'rel':'/r/%s' % self.r, 'limit': '10000'}
            return requests.get('%s%s' % (self.host, 'query'), params=payload).json()
        if self.t == 3:
            payload = {'start':'%s' % self.cid, 'rel':'/r/%s' % self.r, 'limit': '10000'}
            return requests.get('%s%s' % (self.host, 'query'), params=payload).json()              
        if self.t == 4:
            payload = {'end':'%s' % self.cid, 'rel':'/r/%s' % self.r, 'limit': '10000'}
            return requests.get('%s%s' % (self.host, 'query'), params=payload).json()
        if self.t == 5:
            payload = {'language':'%s' % settings.language, 'text':'%s' % self.c, 'limit': '10000'}
            return requests.get('%s%s' % (self.host, 'uri'), params=payload).json()
        
    def get_obj(self):
        return self.obj
        
    def print_obj(self):
        print(self.obj)

    def calculate_weight(self):
        self.weight = 0
        counter = [0,0,0]
        for e in self.obj['edges']:
            if e['start']['language'] == settings.language and e['end']['language'] == settings.language:
                if e['weight']>2.0:
                    counter[0] += 1
                elif e['weight']>=2.0:
                    counter[1] += 1
                elif e['weight']>=1.1:
                    counter[2] += 1
                
#        if counter[0] >= 8:
#            self.weight = 2.1
#            return self.weight
        if counter[1] >= 10:
            self.weight = 2.0
            return self.weight
        if counter[2] >= 5:
            self.weight = 1.1
            return self.weight
        else:
            self.weight = 1.0
            return self.weight
        
    def get_weight(self):
        return self.weight