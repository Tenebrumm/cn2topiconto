# -*- coding: utf-8 -*-

import settings, file_modification
from owlready2 import *

def create_TMPrel(onto):
    with onto:
        class TMPRelated(Thing >> Thing):
            pass

def remove_TMPrel(onto):
    with onto:
        destroy_entity(onto.TMPRelated)

def return_TMPrel(onto):
    TMP_list =[]
    with onto:
        for c in list(onto.classes()):
            for x in c.is_a:
                if type(x) == Restriction and x.property == onto.TMPRelated:
                    if type(x.value) == class_construct.Or:
                        TMP_list.extend([e.name for e in x.value.Classes])
                        # also add subclasses that were possibly created
                        if [e.descendants() for e in x.value.Classes]:
                            for s in [e.descendants() for e in x.value.Classes]:
                                TMP_list.extend([l.name for l in s])
                    else:
                        TMP_list.append(x.value.name)
                        # also add subclasses that were possibly created
                        if [x.value.descendants()]:
                            TMP_list.extend([l.name for l in x.value.descendants()])
                    
    # remove duplicates
    TMP_list = set(TMP_list)
    return list(TMP_list)
    