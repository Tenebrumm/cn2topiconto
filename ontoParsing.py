# -*- coding: utf-8 -*-
"""
Created on Tue Dec  5 13:42:54 2017

@author: katinka
"""
import re
from operator import itemgetter

# add name oclasses for an arbitrary list of triples
def add_name_oclasses(some_list):
    for i in some_list:
        new = remove_articles(' '+i[1]+' ') #removes articles
        new = new.title() #capitalizes words
        new = remove_spaces(new) #removes all spaces
        i[2] = new
    
    # reorder complete list by alphabet again
    some_list = sorted(some_list, key=itemgetter(2))   
    return some_list

def remove_nountag(cid):
    if cid.endswith("/n"):
        cid = re.sub('\/n$', '', cid)
    return cid   

def remove_articles(text): 
    text = re.sub('\s+(a|A|an|An|and|the|The)\s+', '', text)
    text = text.strip() # remove whie spaces at start and end  
    return text
  
def remove_spaces(text):
    text = re.sub(r'\s+', '', text)
    return text