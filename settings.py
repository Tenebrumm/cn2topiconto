# -*- coding: utf-8 -*-
import os.path

language = "en"


namecid = "/c/en/fruit"
name = "fruit"
nameoclass = name.title().replace("_", "") # parsing TC to named concept

dName = "/home/katinka/Dokumente/Master Thesis/Onto/" #CHANGE THIS: set directory path

# optional settings to test filtering suggestions with Numberbatch
# CHANGE to True to experiment with Numberbatch
numberbatch = False
numberbatch_threshold = 0.25

# global file name variables
fName_dic_dump = fName_dic_tax_map = ""
fName_tax = ""

# automatically sets file names
def set_files():
    global fName_dic_dump, fName_dic_tax_map, fName_tax, fName_GR, fName_UR, fName_clean
    
    fName_dic_dump = os.path.join(dName, "%s_dic_dump.json" % name)
    fName_dic_tax_map = os.path.join(dName, "%s_dic_tax_map.json" % name)
    
    fName_tax = os.path.join(dName, "%s_tax.owl" % name) 
    fName_GR = os.path.join(dName, "%s_GR.owl" % name) 
    fName_UR = os.path.join(dName, "%s_UR.owl" % name)
    fName_clean = os.path.join(dName, "%s_clean.owl" % name)
       