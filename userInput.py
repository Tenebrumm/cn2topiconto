# -*- coding: utf-8 -*-
import sys


def choose_from_list(some_list, opt):
    selectall = ("all","a","ALL","A")
    inverse = ("-i", "-I", "not", "NOT")

    while True:
        try:
            print("Select %s: " % opt, end='') 
            
            selection = input()
            # exit
            if selection == "exit":
                raise SystemExit
            # hit enter to continue           
            if not selection:
                return_list = []
                break
            # select all
            elif selection in selectall:
                return_list = list(range(0, len(some_list)))
            # choices entered
            else:
                if selection.startswith(inverse):
                    remove_start = selection.split(" ", 1)
                    remove_list = [int(x) for x in remove_start[1].split()]
                    for n in remove_list:
                        if not n in list(range(0, len(some_list))):
                            print("Some index was not there.")
                    return_list = [x for x in list(range(0, len(some_list))) if x not in remove_list]
                else:
                    return_list = [int(x) for x in selection.split()]
                    
            try: 
                return_list = [some_list[i] for i in return_list]
            
            except IndexError as e:
                print("You chose a number not present.")
                #return to the start of the loop
                continue    
    
        except ValueError as e:
            print("Sorry, I didn't understand that. Enter a number or use TAB to see suggested input options.")
            #return to the start of the loop
            continue
        else:
            #ready to exit the loop.
            break

    return return_list

def print_list_with_index(some_list):
    for c in some_list: 
        # list of oclass
        if type(c) is not list:
            print(str(c)+"[" + str(some_list.index(c)) + "], ", end=' ')
        # CN list of [id, lable, oclass]        
        if type(c) is list:
            print(c[2]+"[" + str(some_list.index(c)) + "], ", end=' ')
    print("\n")

def query_yes_no(question, default = None):
    """Ask a yes/no question via raw_input() and return their answer.

    "question" is a string that is presented to the user.
    "default" is the presumed answer if the user just hits <Enter>.
        It must be "yes" (the default), "no" or None (meaning
        an answer is required of the user).

    The "answer" return value is True for "yes" or False for "no".
    """
    valid = {"yes": True, "y": True, "ye": True,
             "no": False, "n": False}
    if default is None:
        prompt = " [y/n] "
    elif default == "yes":
        prompt = " [Y/n] "
    elif default == "no":
        prompt = " [y/N] "
    else:
        raise ValueError("invalid default answer: '%s'" % default)

    while True:
        sys.stdout.write(question + prompt)
        choice = input().lower()
        if default is not None and choice == '':
            return valid[default]
        elif choice in valid:
            return valid[choice]
        elif choice == "exit":
            raise SystemExit
        else:
            sys.stdout.write("Please respond with 'yes' or 'no' "
                             "(or 'y' or 'n').\n")
                             
