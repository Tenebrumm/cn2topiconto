# -*- coding: utf-8 -*-
"""
Created on Fri Dec  8 15:38:45 2017

@author: katinka
"""
import settings, file_modification
from owlready2 import *
import request, ontoParsing
from userInput import query_yes_no
import itertools


def UR(onto):   
    from TMPrel import remove_TMPrel, create_TMPrel
    from genericRelations import ClassQueue
    from help_output import help_relations
    
    dic_names = file_modification.read_dic_from_file(settings.fName_dic_tax_map)
    dic_dump = file_modification.read_dic_from_file(settings.fName_dic_dump)
    fName_save = settings.fName_UR

    help_relations()
    print("\n")        
    
    with onto:
        # create temporary relation for user to store classes in
        create_TMPrel(onto)
        
        already_asked = []
        mc = onto[settings.nameoclass]
        
        # generate classqueue
        q = ClassQueue()
        class_list = q.return_class_queue(onto, dic_names)
        
        for c in [onto[x] for x in class_list]:
            if c not in already_asked:
                # ancestor or direct subclass of mc
                if c in list(mc.ancestors())+list(mc.subclasses()):
                    #print("case a: sub or super of mc")
                    # ask user, save result
                    print_suggestions(onto, dic_names, dic_dump, c, fName_save)
                    already_asked.append(c)
                    # specifically ask if the user wants to go deeper for everything that is a direct subclass of the main class
                    if c in list(mc.subclasses()):
                        already_asked = ask_for_subclasses(onto, mc, c, dic_names, dic_dump, fName_save, already_asked)
                            
                # a subclass of an ancestor of mc
                elif c in list(itertools.chain.from_iterable([anc.subclasses() for anc in mc.ancestors()])) and c not in mc.ancestors():
                    #("case b: sub of anc of mc")                    
                    # ask user, save result
                    print_suggestions(onto, dic_names, dic_dump, c, fName_save)
                    already_asked.append(c)
                    # specifically ask if the user wants to go deeper for further subclasses
                    already_asked = ask_for_subclasses(onto, mc, c, dic_names, dic_dump, fName_save, already_asked)
                            
                # ancestor of a subclass of mc
                elif c in list(itertools.chain.from_iterable(sub.ancestors() for sub in mc.subclasses())):
                    #print("case c: anc of sub of mc")                    
                    # ask user, save result
                    print_suggestions(onto, dic_names, dic_dump, c, fName_save)
                    already_asked.append(c)
                    # specifically ask if the user wants to go deeper for further subclasses
                    already_asked = ask_for_subclasses(onto, mc, c, dic_names, dic_dump, fName_save, already_asked)
            
                file_modification.save_in_file(onto, fName_save)
                     
        remove_TMPrel(onto)
        
        file_modification.save_in_file(onto, fName_save)
        
def ask_for_subclasses(onto, mc, c, dic_names, dic_dump, fName_save, already_asked):
    if any(csub not in list(mc.ancestors())+list(mc.subclasses()) \
        and csub not in list(itertools.chain.from_iterable([anc.subclasses() for anc in mc.ancestors()])) \
        and csub not in list(itertools.chain.from_iterable(sub.ancestors() for sub in mc.subclasses())) \
        and csub.name in dic_names \
        for csub in c.subclasses()): 
                 
        if query_yes_no("\nThere are further subclasses of %s. Do you want to have suggestions for those?" %c.name):
            # everything was already asked (this should NOT happen!)
            if all(csub in already_asked for csub in list(c.subclasses())):
                    print("All subclasses of %s were already dealt with." %c.name)
            else:
                print("\n>>> Entering subclasses of %s." % c.name)
                for csub in list(c.subclasses()):
                    if not csub in already_asked:
                        # ask user, save result
                        print_suggestions(onto, dic_names, dic_dump, csub, fName_save)
                        file_modification.save_in_file(onto, fName_save)
                        already_asked.append(csub)
                    
                        # recursively call for all further subclasses
                        already_asked = ask_for_subclasses(onto, mc, csub, dic_names, dic_dump, fName_save, already_asked)
                print("<<< Returning from subclasses of %s.\n" % c.name)
                
    return already_asked

#==============================================================================
# gets respective information from CN            
#==============================================================================
            
def get_hasProperty(cid):    
    r = request.CNRequest(cid, 2, "HasProperty")
    obj = r.get_obj()
    
    options_list = []        
    # adds the CN results 
    for e in (obj['edges']):
        try:
            # language has to match
            if e['start']['language'] == settings.language and e['end']['language'] == settings.language:
                if e['start']['@id'] == cid:
                    x = [e['end']['@id'], e['end']['label'], None]
                if e['end']['@id'] == cid:
                    x = [e['start']['@id'], e['start']['label'], None]
                
                if e['weight'] >= r.get_weight() and x not in options_list:
                       options_list.append(x)
               
        except KeyError:
            continue
    
    return(options_list)    

def get_relatedTo(cid):
    r = request.CNRequest(cid, 2, "RelatedTo")
    obj = r.get_obj()
        
    options_list = []
    # adds the CN results to subl
    for e in (obj['edges']):
        try:
            # language has to match
            if e['start']['language'] == settings.language and e['end']['language'] == settings.language: 
                if e['end']['@id'] == cid:                    
                    x = [e['start']['@id'], e['start']['label'], None]
                else: 
                    x = [e['end']['@id'], e['end']['label'], None]

                if e['weight'] >= r.get_weight() and x not in options_list:
                       options_list.append(x)
               
        except KeyError:
            continue
        
    return options_list

def print_suggestions(onto, dic_names, dic_dump, c, fName_save):
    from ontoParsing import add_name_oclasses
    
    if query_yes_no("Do you want suggestions to add axioms for %s?" % c.name):
            
        cid = dic_names[c.name]
        cid = ontoParsing.remove_nountag(cid) # remove nountag if there is one in dict
            
        options_list_prop = []
        options_list_rel = []
        options_list_dump = []
         
        # no CN entity is linked to c
        if cid == "None":
            print("No CN entity is linked to %s." % c.name)
        # get suggestions for c
        else:
            print("-------------------------------------------")
            triples_list = []
            # HasProperty
            options_list_prop = get_hasProperty(cid)
            add_name_oclasses(options_list_prop)
             
            if options_list_prop:
                print([o[2] for o in options_list_prop])
             
            # RelatedTo
            options_list_rel = get_relatedTo(cid)
            add_name_oclasses(options_list_rel)
             
            if options_list_rel:
                print([o[2] for o in options_list_rel])
                 
            # Dump
            # only if something was set aside for later
            if c.name in dic_dump and dic_dump[c.name]:
                options_list_dump = [x[2] for x in dic_dump[c.name]]
                print(options_list_dump)
             
            # suggestion triples
            triples_list = options_list_prop + options_list_rel
            for k in dic_dump.keys():
                triples_list.extend(dic_dump[k])
         
        print("-------------------------------------------\n")
        
        if not options_list_prop+options_list_rel+options_list_dump:
            print("It seems like no suggestions were found.")
            
            if query_yes_no("Do you still want to add relations to %s" % c.name):
                ask_user_rel(onto, c.name, options_list_prop, options_list_rel, options_list_dump, dic_names, triples_list, fName_save)                
        else:
            ask_user_rel(onto, c.name, options_list_prop, options_list_rel, options_list_dump, dic_names, triples_list, fName_save)
            
            
def ask_user_rel(onto, c, options_list_prop, options_list_rel, options_list_dump, dic_names, triples_list, fName_save):
    from userInput import query_yes_no
    from createOnto import create_subclass_of_super, create_class
    from autocomplete import suggest_relation, suggest_options, suggest_instances, suggest_subclasses
    from TMPrel import return_TMPrel

    suggestions = [o[2] for o in options_list_prop]+[o[2] for o in options_list_rel]+options_list_dump    
    
    if query_yes_no("\nGenerate a (new) axiom?"):
        
        while True:
            try:
                rel = suggest_relation(onto) # IsA and TMPRel will also be suggested
                if rel == "exit":
                    raise SystemExit
                # case I: more than one relation was entered
                elif len(rel.split()) > 1:
                    if any(r.endswith("-") for r in rel.split()):
                        print("Inverted Axioms for multiple relations are not possible yet. Continuing.")
                    # list of lists that stores relname and associated concepts
                    rel_or_list = []
                    for r in rel.split():
                        # relation entered already exists and is not TMPRelated or IsA
                        if onto[r] in list(onto.object_properties()) and r != "TMPRelated" and r != "IsA":
                            possible_inst_list = []
                            possible_subc_list = []
                            for s in onto[r].range:
                                # if the range was extended Thing do not suggest it
                                if s != owl["Thing"]:
                                    if list(s.instances()):
                                        possible_inst_list.extend(i.name for i in s.instances())
                                    if s.subclasses():
                                        possible_subc_list.extend(i.name for i in s.subclasses())
                            # instances were found
                            if possible_inst_list or possible_subc_list:
                                con = suggest_options(c, r, possible_inst_list + possible_subc_list + [s.name for s in onto[r].range if s != owl["Thing"]])
                            # already occuring relation was entered but range had no instances or subclasses
                            else:
                                con = suggest_options(c, r, suggestions + list(return_TMPrel(onto)) + [i.name for i in list(onto.individuals())])
                        # complete new relation name was entered
                        else:                     
                            con = suggest_options(c, r, suggestions + list(return_TMPrel(onto)) + [i.name for i in list(onto.individuals())])
                        
                        # case 1 & case 2: create relation and class(es) or axioms for existing instanc(es)
                        # if no () or : occur
                        if check_con(con):
                            # one or more already existing instances were entered
                            if check_instances_entered(onto, con):
                                rel_or_list.append([r, [c for c in con.split()]])
                            
                            # new or existing concepts were entered
                            else:
                                # more than one concept entered
                                if len(con.split()) > 1:
                                    for x in con.split():
                                        create_class(onto, x)
                                        rel_or_list.append([r, [x for x in con.split()]])
                                # single concept entered
                                else:
                                    create_class(onto, con)
                                    rel_or_list.append([r, [con]])
    
                        # if con is only one concept and : or () occur
                        if not " " in con and not check_con(con):     
                            # case 3: create relation and instances of a class
                            if con.endswith("()"):
                                # remove brackets if instances should be created
                                con = con[:-2]
                                ins = suggest_instances(con, suggestions + list(return_TMPrel(onto)) + [i.name for i in list(onto.individuals())])
                                ins = check_further_dynamic_extensions(ins)
                                create_class(onto, con)
                                create_instances(onto, con, ins)
                                rel_or_list.append([r, [con]])       
                                
                            # case 4: create relation and add subclasses of a class
                            if con.endswith(":"):
                                # remove :
                                con = con[:-1]
                                create_class(onto, con)
                                rel_or_list.append([r, [con]])
                                
                                sub = suggest_subclasses(con, suggestions)
                                sub = check_further_dynamic_extensions(sub)

                                for s in sub.split():                        
                                    create_subclass_of_super(onto, s, con)
                                # make subclasses disjoint
                                make_disjoint(onto, con, [s for s in sub.split()])
    
                        # if con consists of more concepts and : or () occur
                        if " " in con and not check_con(con):
                            for x in con.split():
                                # case 3: create relation and instances of a class
                                if x.endswith("()"):
                                    # remove brackets if instances should be created
                                    x = x[:-2]
                                    ins = suggest_instances(x, suggestions + list(return_TMPrel(onto)) + [i.name for i in list(onto.individuals())])
                                    ins = check_further_dynamic_extensions(ins)
                                    create_class(onto, x)
                                    create_instances(onto, x, ins)
                                    
                                # case 4: create relation and add subclasses of a class
                                if x.endswith(":"):
                                    # remove :
                                    x = x[:-1]
                                    sub = suggest_subclasses(x, suggestions)
                                    sub = check_further_dynamic_extensions(sub)

                                    for s in sub.split():                        
                                        create_subclass_of_super(onto, s, x)
                                    # make subclasses disjoint
                                    make_disjoint(onto, x, [s for s in sub.split()])
                                # if x has no () or :
                                else:
                                    create_class(onto,x)
                                    
                            # remove () and :
                            con = con.replace("()", " ")
                            con = con.replace(":", " ")
                            
                            rel_or_list.append([r, [x for x in con.split()]])
                    
                    
                        # exit
                        if con == "exit":
                            raise SystemExit 
                        if not con:
                            rel_or_list = []
                    
                    # create the axiom
                    add_rel_or_expression(onto, c, rel_or_list)
                    
                # case II: single relation was entered
                elif rel:
                    # add simple "inverse" axiom
                    if rel.endswith("-"):
                        # remove :
                        rel = rel[:-1]
                        con = suggest_options(c, rel, suggestions + list(return_TMPrel(onto)) + [i.name for i in list(onto.individuals())], True)
                        # more than one concepts, generate multiple axioms
                        if len(con.split()) > 1:
                            for x in con.split():
                                create_class(onto, x)
                                # SubClassOf
                                if rel == "IsA":
                                    if onto[con] in list(onto.individuals()):
                                        create_instances(onto, c, con)
                                    else:
                                        IsA_inverted_map(onto, x, c, dic_names, triples_list)
                                else:
                                    generate_rel(onto, x, rel, c)
                        else:
                            if con:
                                create_class(onto, con)
                                # SubClassOf
                                if rel == "IsA":
                                    if onto[con] in list(onto.individuals()):
                                        create_instances(onto, c, con)
                                    else:
                                        IsA_inverted_map(onto, con, c, dic_names, triples_list)
                                else:
                                    generate_rel(onto, con, rel, c)
                        
                        if con == "exit":
                            raise SystemExit
                            
                    # all other types of axiom
                    else:
                        # relation entered already exists and is not TMPRelated or IsA
                        if onto[rel] in list(onto.object_properties()) and rel != "TMPRelated"and rel != "IsA":
                            possible_inst_list = []
                            possible_subc_list = []
                            for r in onto[rel].range:
                            # if the range was extended Thing do not suggest it
                                if r != owl["Thing"]:
                                    if list(r.instances()):
                                        possible_inst_list.extend(i.name for i in r.instances())
                                    if r.subclasses():
                                        possible_subc_list.extend(i.name for i in r.subclasses())
                            # instances or subclasses were found
                            if possible_inst_list or possible_subc_list:
                                con = suggest_options(c, rel, possible_inst_list + possible_subc_list + [r.name for r in onto[rel].range if r != owl["Thing"]])
                            # already occuring relation was entered but range had no instances or subclasses
                            else:
                                con = suggest_options(c, rel, suggestions + list(return_TMPrel(onto)) + [i.name for i in list(onto.individuals())])
                        # complete new relation name was entered
                        else:                     
                            con = suggest_options(c, rel, suggestions + list(return_TMPrel(onto)) + [i.name for i in list(onto.individuals())])
                        
                        # case 1 & case 2: create relation and class(es) or axioms for existing instanc(es)
                        # if no () or : occur
                        if check_con(con):
                            # one or more already existing instances were entered
                            if check_instances_entered(onto, con):
                                create_instance_axiom(onto, c, rel, None, con)
                            
                            # new or existing concepts were entered
                            else:
                                # more than one concept entered
                                if len(con.split()) > 1:
                                    for x in con.split():
                                        create_class(onto, x)
                                    # SubClassOf
                                    if rel == "IsA":
                                        add_isa_or_expression(onto, c, rel, con)
                                    else:
                                        add_or_expression(onto, c, rel, con)
                                # single concept entered
                                else:
                                    # SubClassOf
                                    if rel == "IsA":
                                        IsA_map(onto, c, con, dic_names, triples_list)                                            
                                    else:
                                        generate_rel(onto, c, rel, con)
    
                        # if con is only one concept and : or () occur
                        if not " " in con and not check_con(con):     
                            # case 3: create relation and instances of a class
                            if con.endswith("()"):
                                # remove brackets if instances should be created
                                con = con[:-2]
                                ins = suggest_instances(con, suggestions + list(return_TMPrel(onto)) + [i.name for i in list(onto.individuals())])
                                ins = check_further_dynamic_extensions(ins)
                                create_class(onto, con)
                                create_instances(onto, con, ins)
                                
                                #create axiom
                                if rel != "TMPRelated" and rel != "IsA": 
                                    if query_yes_no("Create an existential axiom for " + c + "?" ):
                                        instance = suggest_options(c, rel, [i.name for i in list(onto[con].instances())])
                                        if instance =="exit":
                                            raise SystemExit
                                        create_instance_axiom(onto, c, rel, con, instance)
                                    else:
                                        # dynamically create relation with range set
                                        NewClass = types.new_class(rel, (Thing >> onto[con],))
                                elif rel == "IsA":
                                    IsA_map(onto, c, con, dic_names, triples_list)                               
                                
                            # case 4: create relation and add subclasses of a class
                            if con.endswith(":"):
                                # remove :
                                con = con[:-1]
                                #generate_rel(onto, c, rel, con)
                                sub = suggest_subclasses(con, suggestions + list(return_TMPrel(onto)))
                                sub = check_further_dynamic_extensions(sub)
                                create_class(onto, con)
                                for s in sub.split():
                                    print(sub.split())
                                    create_subclass_of_super(onto, s, con)
                                # make subclasses disjoint
                                make_disjoint(onto, con, [s for s in sub.split()])
                                                                  
                                #create axiom
                                if rel != "TMPRelated" and rel != "IsA": 
                                    if query_yes_no("Create an existential axiom for " + c + "?" ):
                                        subc = suggest_options(c, rel, [i.name for i in list(onto[con].subclasses())] + [con])
                                        if subc == "exit":
                                            raise SystemExit
                                        create_subc_axiom(onto, c, rel, con, subc)
                                    else:
                                        # dynamically create relation with range set
                                        NewClass = types.new_class(rel, (Thing >> onto[con],))
                                        
                                elif rel == "IsA":
                                    IsA_map(onto, c, con, dic_names, triples_list)
    
                        # if con consists of more concepts and : or () occur
                        if " " in con and not check_con(con):
                            for x in con.split():
                                # case 3: create relation and instances of a class
                                if x.endswith("()"):
                                    # remove brackets if instances should be created
                                    x = x[:-2]
                                    ins = suggest_instances(x, suggestions + list(return_TMPrel(onto)) + [i.name for i in list(onto.individuals())])
                                    ins = check_further_dynamic_extensions(ins)
                                    create_class(onto, x)
                                    create_instances(onto, x, ins)
                                    
                                # case 4: create relation and add subclasses of a class
                                if x.endswith(":"):
                                    # remove :
                                    x = x[:-1]
                                    sub = suggest_subclasses(x, suggestions)
                                    sub = check_further_dynamic_extensions(sub)
                                    for s in sub.split():                        
                                        create_subclass_of_super(onto, s, x)
                                    # make subclasses disjoint
                                    make_disjoint(onto, x, [s for s in sub.split()])
                                
                                # if x has no () or :
                                else:
                                    create_class(onto,x)
                                    
                            # remove () and :
                            con = con.replace("()", " ")
                            con = con.replace(":", " ")
                            
                            # SubClassOf
                            if rel == "IsA":
                                add_isa_or_expression(onto, c, rel, con)
                            else:
                                add_or_expression(onto, c, rel, con)

                    # exit
                    if con == "exit":
                        raise SystemExit
                    if not con:
                        continue
                # nothing entered    
                if not rel:
                    return
                # no concept entered
                if not con:
                    continue
                
                file_modification.save_in_file(onto, fName_save)
                file_modification.save_dic_in_file(dic_names, settings.fName_dic_tax_map)
                    
            except ValueError as e:
                print("Sorry, I didn't understand that.")

# handles cycles created by IsA
def handle_cycle(onto, con1, con2):
    import itertools
    with onto:
        if con1 == con2:
            return
        if query_yes_no("You created a cycle. Make %s and %s equivalent?" % (onto[con1].name, onto[con2].name)):
            onto[con2].equivalent_to.append(onto[con1])
            onto[con1].equivalent_to.append(onto[con2])
            # if multiple intermediate concepts are affected            
            if not onto[con2] in list(onto[con1].is_a) and not onto[con1] in list(onto[con2].is_a):
                # get inbetween concepts
                inbetween_list = []
                for x in onto.classes():
                    if x in onto[con2].descendants() and x in onto[con1].ancestors(): 
                        inbetween_list.append(x)
                for e1,e2 in itertools.combinations(inbetween_list,2):
                    e1.equivalent_to.append(e2)
                    e2.equivalent_to.append(e1)
            # remove is_a if concepts are equivalent
            for c in list(onto.classes()):
                if c.equivalent_to:
                    set_is_a = set(c.is_a)
                    for x in c.equivalent_to:
                        if x in set_is_a:
                            set_is_a.remove(x)
                    c.is_a = list(set_is_a)
            print("Cycles are complicated. If you want to add further axioms that use %s, please EXIT and restart the current step, so the equivalence can take effect." % con1)

# defines mapping for IsA
def IsA_map(onto, c, con, dic_names, triples_list):
    from createOnto import create_class
    with onto:
        # con exists
        if onto[con] in list(onto.classes()):
            # inheritance cycle
            if onto[con] in onto[c].descendants():
                handle_cycle(onto, con, c)
            else:
                onto[c].is_a.append(onto[con])
            # id exists
            if con in dic_names:
                pass
            else:
                # add to dic_tax_map, if no respective id could be found the dic entry value is None
                CNid = str(get_CNid(onto, onto[con], triples_list))
                if CNid in dic_names:
                    pass
                else:
                    dic_names.update({con:CNid})
                
        else:
            create_class(onto, con)
            onto[c].is_a.append(onto[con])
            # add to dic_tax_map, if no respective id could be found the dic entry value is None
            CNid = str(get_CNid(onto, onto[con], triples_list))
            dic_names.update({con:CNid})
            
def IsA_inverted_map(onto, con, c, dic_names, triples_list):
    from createOnto import create_class
    with onto:
        # con exists
        if onto[con] in list(onto.classes()):
            # inheritance cycle
            if onto[c] in onto[con].descendants():
                handle_cycle(onto, c, con)
            else:
                onto[con].is_a.append(onto[c])
            # id exists
            if con in dic_names:
                pass
            else:
                # add to dic_tax_map, if no respective id could be found the dic entry value is None
                CNid = str(get_CNid(onto, onto[con], triples_list))
                if CNid in dic_names:
                    pass
                else:
                    dic_names.update({con:CNid})
                
        else:
            create_class(onto, con)
            onto[c].is_a.append(onto[con])
            # add to dic_tax_map, if no respective id could be found the dic entry value is None
            CNid = str(get_CNid(onto, onto[con], triples_list))
            dic_names.update({con:CNid})

def check_further_dynamic_extensions(item_list):
    item_list = item_list.split()
    if any(item.endswith(("()", ":")) for item in item_list):
        item_list_new = []
        print("You can not add further dynamic extensions.  Suffixes are removed and names created without them.")
        for i in item_list:
            if i.endswith("()"):
                i = i[:-2]
                item_list_new.append(i)
            elif i.endswith(":"):
                i = i[:-1]
                item_list_new.append(i)
            else:
                item_list_new.append(i)
        item_list_new = ' '.join(item_list_new)
        return item_list_new
    else:
        item_list= ' '.join(item_list)
        return item_list 

# checks for con that was entered if there should be subclasses or instances created for any of the concepts in con
def check_con(con):
    if con:
        for x in con.split():
            if x.endswith("()") or x.endswith(":"):
                return False
        return True

# returns the CN id for a suggestion
def get_CNid(onto, oclass, triples_list):
    import re
    
    for e in triples_list:
        if oclass.name == e[2]:
            return e[0]
        else:
            continue
        
    print("There was no respective entity for %s. We are trying to find the most suitable." % oclass.name)
    #==============================================================================
    # Could implement a function here that tries to find an appropriate CN id for
    # any concept name entered!             
    #==============================================================================
    n = oclass.name
    incl_words = re.findall('[A-Z][^A-Z]*', n)
    n_search = '+'.join(incl_words)
    r = request.CNRequest(None, 5, None, None, n_search)
    obj = r.get_obj()
    try:
        r_test = request.CNRequest(obj['@id'], 1, None, 1, None)
        obj_test = r_test.get_obj()
        try: 
            obj_test['error']
            print("No entity was found.")
            return None
        except KeyError:
            if query_yes_no("Store %s as respective CN id?" % obj['@id']):
                return obj['@id']
            else:
                return None
    except KeyError:
        print("No entity was found.")
        return None
#==============================================================================
# Functions for Generation of relations, instances and complex axioms    
#==============================================================================

# dynamically generated user entered relations in onto
# con is only one concept
def generate_rel(onto, c, rel, con):    
    from createOnto import create_class
    with onto:
        # dynamically create relation
        NewClass = types.new_class(rel, (Thing >> Thing,))
        # creates new class and adds the axiom
        create_class(onto, con)
        onto[c].is_a.append(onto[rel].some(onto[con]))

# generates an or expression with the contents from con and adds an existential axiom to c
def add_or_expression(onto, c, rel, con):
    with onto:
        # dynamically create relation
        NewClass = types.new_class(rel, (Thing >> Thing,))
        # adds the complex axiom
        or_list = []
        ins_list = []
        
        for x in con.split():
            if x in [i.name for i in list(onto.individuals())]:
                    ins_list.append(onto[x])
            else:
                or_list.append(onto[x])
        if ins_list:
            ins = OneOf(ins_list)
            or_list.append(ins)
        
        res = class_construct.Or(or_list)  
        onto[c].is_a.append(onto[rel].some(res))

# generates an or expression for IsA with the contents from con and adds an existential axiom to c
def add_isa_or_expression(onto, c, rel, con):
    with onto:
        or_list = []
        ins_list = []
        
        for x in con.split():
            if x in [i.name for i in list(onto.individuals())]:
                    ins_list.append(onto[x])
            else:
                or_list.append(onto[x])
        if ins_list:
            ins = OneOf(ins_list)
            or_list.append(ins)
        
        res = class_construct.Or(or_list)  
        onto[c].is_a.append(res)    

# rel_or_list is a list of all subparts of the axiom of the form [[rel1, [con11, con12, con13]], [rel2, [con21, con22]]]]
def add_rel_or_expression(onto, c, rel_or_list):
    if not rel_or_list:
        return
    else:
        with onto:
            #print("The list")
            #print(rel_or_list)
            # dynamically create relation
            for r in [l[0] for l in rel_or_list]:
                if r != "IsA":
                    NewClass = types.new_class(r, (Thing >> Thing,))
            
            # adds the complex axiom
            or_list = []
    
            for el in rel_or_list:
                rel = el[0]
                ins_list = []
                cl_list = []
                # check for instances to add those as OneOf
                for x in el[1]:
                    if x in [i.name for i in list(onto.individuals())]:
                        ins_list.append(onto[x])
                    else:
                        cl_list.append(onto[x])
                if ins_list:
                    ins = OneOf(ins_list)
                    cl_list.append(ins)
                #print(ins_list)
                #print(cl_list)
                y = class_construct.Or(cl_list)
                #print(y)
                if rel == "IsA":
                    or_list.append(y)
                else:
                    or_list.append(onto[rel].some(y))
                #print(or_list)
                    
            onto[c].is_a.append(class_construct.Or(or_list))
        

# create instances when a new class is created
def create_instances(onto, con, ins):
    with onto:
        for i in ins.split():
            # creates a new instance in onto
            new_instance = onto[con](i.lower())

def create_instance_axiom(onto, c, rel, con, ins):
#    if rel == "IsA":
#        print("This is not a permitted axiom.")
#        return
        
    ins_list = []
    # if multiple individuals are added
    with onto:
        # dynamically create relation
        # relation does not exist yet
        if con:
            NewClass = types.new_class(rel, (Thing >> onto[con],))
        else:
            NewClass = types.new_class(rel, (Thing >> Thing,))
        
        for i in ins.split():
            if i in [i.name for i in list(onto.individuals())]:
                ins_list.append(onto[i])
            else:
                print("Something you added was not an individual. The axiom was added without it.")
        # at least one instance was added
        if ins_list:
            if rel == "IsA":
                onto[c].is_a.append(OneOf(ins_list))
            else:    
                onto[c].is_a.append(onto[rel].some(OneOf(ins_list)))  

def create_subc_axiom(onto, c, rel, con, subc, no_rr=False):
    from createOnto import create_class
    subc_list = []
    # if multiple subclasses are added
    with onto:
        # dynamically create relation
        # relation does not exist yet
        if con and no_rr == False:
            NewClass = types.new_class(rel, (Thing >> onto[con],))
        else:
            NewClass = types.new_class(rel, (Thing >> Thing,))
        
        for i in subc.split():
            if i in [i.name for i in list(onto[con].descendants())]:
                subc_list.append(onto[i])
            else:
                print("Some concept you added is not a subclass of %s. Just a warning. The axiom was still added as specified." % con)
                create_class(onto, i)
                subc_list.append(onto[i])
        # at least one subclass was added
        if subc_list:
            onto[c].is_a.append(onto[rel].some(class_construct.Or(subc_list)))  

# check if everything entered was an instance
def check_instances_entered(onto, con):
    for i in con.split():
        if i in [i.name for i in list(onto.individuals())]:
            pass
        else:
            return False
    return True

# makes classes disjoint
def make_disjoint(onto, con, subclass_list):
    with onto:
        # make the selected disjoint if more than one
        if len(subclass_list)>1:
            AllDisjoint([onto[s] for s in subclass_list])
        # if only one make disjoint from all existing
        elif len(subclass_list)>0:
            x = subclass_list[-1]
            con_subc = [s.name for s in onto[con].subclasses() if s.name != x]
            # disjoint from all other subclasses
            if len(con_subc)>1:
                if query_yes_no("Make %s disjoint from all existing subclasses? (%s)" % (x, ', '.join(con_subc)) ):
                    for s in con_subc:
                        AllDisjoint([onto[x],onto[s]])
                # not disjoint
                else:
                    pass
