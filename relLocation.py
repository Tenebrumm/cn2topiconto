# -*- coding: utf-8 -*-

from owlready2 import *
import createCNstorage, prepareSuggestions
import genericRelationsInput
from userInput import query_yes_no

def run_location(onto, dic_tax_map, dic_dump, fName_save):    
    loc = createCNstorage.CNstorage(onto, "AtLocation", "location", False) # HERE: change to False
    # loc.print_CNdics()
    loc_sug = prepareSuggestions.Suggestions(onto, loc, "location")
    #loc_sug.print_suggestions()

    print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
    if query_yes_no("Do you want suggestions of the form \"C AtLocation x\"? (locations for C)"):    
        genericRelationsInput.suggest_type(onto, dic_tax_map, dic_dump, loc_sug, fName_save, "right", "locations")
    print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>") 
    if query_yes_no("Do you want suggestions of the form \"x AtLocation C\"? (things located at C)"):       
        genericRelationsInput.suggest_type(onto, dic_tax_map, dic_dump, loc_sug, fName_save, "left", "location")    
    else:
        print("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<")
