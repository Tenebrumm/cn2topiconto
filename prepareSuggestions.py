# -*- coding: utf-8 -*-
import settings, file_modification
from owlready2 import *
import request, ontoParsing

class Suggestions:
    
    def __init__(self, onto, CNstore, phrase):
        self.phrase = phrase
        self.suggestions_dic_left = {}
        self.suggestions_dic_right = {}
    
        dic_tax_map = file_modification.read_dic_from_file(settings.fName_dic_tax_map)
        Suggestions.build_suggestions(self, onto, CNstore, dic_tax_map)
                

    # combines suggestions from all CN results for descendants    
    def build_suggestions(self, onto, CNstore, dic_tax_map):        
        with onto:
            for c in list(onto.classes()):
                if c.name in dic_tax_map:
                    #print("Looking at ", c.name)
                    #print(sorted(list(e.name for e in c.descendants())))
                    for s in c.descendants():
                        if s.name in CNstore.CN_dic_left:
                            #print("Adding ", s.name)
                            if not c.name in self.suggestions_dic_left:
                                self.suggestions_dic_left[c.name] = []
                            self.suggestions_dic_left[c.name].extend([x for x in CNstore.CN_dic_left[s.name] if x[2] not in [y[2] for y in self.suggestions_dic_left[c.name]]])
                            #print(self.suggestions_dic_left[c.name])
                        if s.name in CNstore.CN_dic_right:
                            #print("Adding ", s.name)
                            if not c.name in self.suggestions_dic_right:
                                self.suggestions_dic_right[c.name] = []
                            self.suggestions_dic_right[c.name].extend([x for x in CNstore.CN_dic_right[s.name] if x[2] not in[y[2] for y in self.suggestions_dic_right[c.name]]])
                            #print(self.suggestions_dic_right[c.name])

#        # remove duplicates
#        for key, values in self.suggestions_dic_left.items():
#            if values:
#                values_no_dup = set(tuple(i) for i in values)
#                self.suggestions_dic_left[key] = values_no_dup
#            #print(key, sorted([x[2] for x in values]))
#            
#        for key, values in self.suggestions_dic_right.items():
#            if values:
#                values_no_dup = set(tuple(i) for i in values)
#                self.suggestions_dic_right[key] = values_no_dup
#            #print(key, sorted([x[2] for x in values]))     
        
    
    def print_suggestions(self):
        for key, values in self.suggestions_dic_left.items():
            print(key, sorted([x[2] for x in values]))
        for key, values in self.suggestions_dic_right.items():
            print(key, sorted([x[2] for x in values])) 
            
    
    def restrict_suggestions(self, onto, c, del_dic, used_dic, spec_dic, direction):
        from createOnto import get_superclasses        
        
        del_list = []
        used_list = []
        spec_list = []
        rs_left = []
        rs_right = []
        
        with onto:
            for key in del_dic:
                # collect everything that was deleted at the start or already used for an axiom
                del_list.extend(del_dic[key])
            for a in get_superclasses(onto, c, True) + [onto[c]]:
                if a.name in used_dic:
                    used_list.extend(used_dic[a.name])
                if a.name in spec_dic:
                    # collect everything already used as subclass or in an OR construction axiom for any superclass
                    spec_list.extend(spec_dic[a.name])
            
            # update suggestions only if there is a key (exist suggestions)
            if direction == "left":
                if c in self.suggestions_dic_left:
                    rs_left = [x[2] for x in self.suggestions_dic_left[c] if x[2] not in del_list+used_list+spec_list]
                return del_list, used_list, spec_list, rs_left
        
            if direction == "right":
                if c in self.suggestions_dic_right:
                    rs_right = [x[2] for x in self.suggestions_dic_right[c] if x[2] not in del_list+used_list+spec_list]  
                return del_list, used_list, spec_list, rs_right


                
            
            