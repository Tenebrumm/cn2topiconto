# -*- coding: utf-8 -*-
import json
import os
from owlready2 import *

def save_in_file(onto, fName):
    #clearing file and saving in file
    def deleteContent(fName):
        with open(fName, "w"):
            pass
    onto.save(file=fName)

# read the dictonary from file
def read_dic_from_file(fName_dic):
    dic = json.load(open(fName_dic))
    return dic

# save the dictonary in file
def save_dic_in_file(dic, fName_dic):
     with open(fName_dic, "w") as f:
         f.write(json.dumps(dic))     

# delete file
def rm_file(fName):
    os.remove(fName)
    
def oprint_from_file(fName):
    onto = get_ontology("file://%s" % fName).load()    
        
    print("Classes") 
    print(list(onto.classes()))
    try:
        for c in list(onto.classes()):
            if len(c.subclasses()):
                print("Subclasses of", c)
                print(c.subclasses())
#            print("Superclasses of", c )
#            print(c.ancestors())
    except AttributeError:
        None        