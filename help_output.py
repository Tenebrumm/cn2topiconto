# -*- coding: utf-8 -*-
from userInput import query_yes_no

def help_suggestions():
    if query_yes_no("Do you need HELP understanding the suggestions?"):
        print("\n------------ What do the suggestions mean? ------------")
        print("In this step you can decide to add relations and axioms for three types of information.")
        
        print("Suggestions for each class are pooled over all of its subclasses.")
        print("This way concepts higher up will have more suggestions, but they suggestions get more specific the lower you get.")
        print("For each toplevel concept you can \"throw out\" suggestions. Those will not show up again for any subclass.")
        print("You can use it to remove misspelt words or nonsense suggestions.")
        print("If you add an axiom the suggestion you added will not show up below anymore as it is naturally inherited.")
        print("Creating subclasses or disjunctive axioms puts concepts that were already used into a seperate list that might be suitable for specification further below.")        
        
        print("Type HasPart: You can not chose a relation name. HasPart and PartOf will be created automatically.")
        print("You are given the choice to add a universal axioms of the form Car SubClassOf HasPart only CarParts.")
        print("Type Location: Choose a relation name that best fits the information.")
        print("Type Capabilities: Choose a relation name that best fits the information.")

        print("For each type you can choose if you want to have suggestions for each direction.")
        print(". RelationTo C or C RelationTo . , where C is the concept currently considered.")
        print("Often some ones direction does not make sense for the considered domain.")
        print("----------------------------------------------------------")

def help_relations():
    if query_yes_no("Do you need HELP creating relations and axioms?"):
        print("\n------------ How to add additional relations and axioms ------------")
        print("There are multiple different ways to specify relations.")
        print("After you entered the relation name you can add one or multiple concepts.")
        print("All created axiom that do not use the role nam IsA or TMPRelated are existential!")
        
        print("\nOption 1: If you enter a single concept name, then an existential axiom will be created.")
        print("What you entered: HumanBody.NeedsToSurvive.Food")
        print("What is created: HumanBody SubClassOf NeedsToSurvive some Food")
        
        print("\nOption 2: If you enter multiple concept names, an axiom with a disjunction will be created.")
        print("What you entered: House.IsUsedAs.Shelter LivingPlace Home")        
        print("What is created: House SubClassOf IsUsedAs some (Shelter OR LivingPlace OR Home)")
        print("\nNote: If you want to add a conjunctive axiom you have to enter the axioms seperately.")
        
        print("\nDynamic Extension with instances: If one or multiple of the entered concepts ends with () you will be asked to enter instances for these concept next.")
        print("What you entered: Apple.HasColor.Color()")
        print("Enter individuals: green red yellow blue pink")
        print("In the next step you can add an existential axiom with one or multiple of those instances.")
        print("Apple.HasColor.green red yellow")
        print("What is created: Apple SubClassOf HasColor {green, red yellow}")
        print("Apple now has as individuals: green red yellow blue pink. Those can be used directly to create further axioms.")
        print("The range of HasColor is automatically restricted to Color.")
        
        print("\nDynamic Extension with subclasses: If one or multiple of the entered concepts ends with : you will be asked to enter subclasses for these concept next.")
        print("What you entered: House.HasRoom.Room:")
        print("Enter subclasses: LivingRoom Kitchen Bathroom Study")
        print("What is created: House SubClassOf HasRoom some Room.")
        print("Room now has subclasses: LivingRoom Kitchen Bathroom Study.")
        print("The range of HasRoom is automatically restricted to Room.")

        print("\nBoth () and : can be used at the same time and combined arbitrarily often.")
        
        print("If multiple relation names are given, multiple prompts follow and the separate existential parts will be combined into a disjunctive axiom.")
        print("Range restrictions are only made if only ONE relation name is given.")        
        
        print("\nThe IsA role is automatically created at the start.")
        print("It can be used to add new subsumptions to the taxonomy.")
        print("Input works the same as for any other relation and combinations and dynamic extensions are possible.")      
        
        print("\nThe TMPRelation role is automatically created at the start.")
        print("It can be used to add a concept to the taxonomy without any specific relation.")
        print("During creation of User Specified Relations it can also be used to remember a suggestion for a later concept.")
        print("There, everything so far connected with TMPRelation, will be also offered under the TAB suggestions.")        
        print("All created axioms with TMPRelation will be destroyed at the end and only classes or instances added with its help remain.")
        print("---------------------------------------------------------------------")
        
        print("\nCommon relations are HasColor, HasMaterial, HasTaste, HasSize or more general UsedFor, HasType.\n")