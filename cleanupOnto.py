import settings, file_modification
from owlready2 import *

#==============================================================================
# cleanup the ontology
#==============================================================================

def nest_classes(onto, fName_save):
    from userInput import query_yes_no
    from createOnto import get_superclasses    
    
    print("This step offers the option to clean up the taxonomy for all newly created atomic concepts.")  
    print("In the following you can nest existing concepts or create new ones by entering a new concept name.\n")
    print("New concept names specified as superclass will be added at the current level.")     
    print("You can always press TAB to see all atomic concepts at your current level.")
    
    with onto:    
        not_done = True
        while not_done:
            # update unnested classes
            unnested_classes = []
            for c in list(onto.classes()):
                # no superclasses
                if not get_superclasses(onto, c.name, True):
                    unnested_classes.append(c.name)
            print("\nThese are all concepts at the top level at the moment (only subclass of Thing).") 
            print(sorted(unnested_classes))
            
            jump = "Thing"
            previous_jump = None
            if query_yes_no("\nDo you want to nest concepts on the current level?"):
                nest_classes_at_level(onto, fName_save, jump, unnested_classes)
            if query_yes_no("\nDo you want to go to a deeper level to do further nesting?"):
                nest_classes_deeper(onto, jump, previous_jump, fName_save)
            else:
                not_done = False
                file_modification.save_in_file(onto, fName_save)
                break

# nest classes that were added through relations and create appropriate sub and superclass hierarchy
def nest_classes_at_level(onto, fName_save, jump, unnested_classes):
    from createOnto import get_superclasses, create_class, check_inheritance_cycle, create_subclass_of_super, get_direct_superclasses
    from userInput import query_yes_no
    from autocomplete import suggest_subclasses, suggest_superclasses
    
    with onto:
        while query_yes_no("\nDefine a new (atomic) concept inclusion?"): 
            superc = suggest_superclasses(unnested_classes)
            # exit
            if superc == 'exit':
                raise SystemExit
                
            subc = suggest_subclasses(superc, unnested_classes)
            # exit
            if subc == 'exit':
                raise SystemExit
            
            # same input for sub and sup
            sup_list = superc.split()
            sub_list = subc.split()
            if not set(sup_list).isdisjoint(set(sub_list)):            
                print("This is not a valid input.")               

            else:
                # create classes and nesting
                for sup in sup_list:
                    
                # warn if it exists not at the current level
                    if onto[sup] in list(onto.classes()) and sup not in unnested_classes:
                        print("Warning: %s already exists somewhere else in the ontology." % sup)
                        if query_yes_no("Add an inclusion for the current level?"):
                            if not check_inheritance_cycle(onto, jump, sup):
                                create_subclass_of_super(onto, sup, jump)
                        else:
                            pass
                            
                    for s in sub_list:
                            
                        # create if does not exist
                        if onto[sup] not in list(onto.classes()):
                            # at top level
                            if jump == "Thing":
                                create_class(onto, sup)
                            # at current level
                            else:
                                create_subclass_of_super(onto, sup, jump)
                        
                        create_class(onto, s)
                        
                        # add axiom
                        if not check_inheritance_cycle(onto, s, sup):
                            onto[s].is_a.append(onto[sup])
                            # remove old axiom    
                            if onto[jump] in onto[s].is_a:
                                onto[s].is_a.remove(onto[jump])
    
                        
                        # update unnested_classes list 
                        # add newly created concepts to list
                        if jump == "Thing":
                            if not get_superclasses(onto, sup, True):
                                unnested_classes.append(sup)
                        else:
                            if onto[jump] in get_direct_superclasses(onto, sup, True):
                                unnested_classes.append(sup)
                            
                        # remove now subconcepts
                        if s in unnested_classes:
                            unnested_classes = set(unnested_classes)
                            unnested_classes.remove(s)
                            unnested_classes = list(unnested_classes)   
                        
                        file_modification.save_in_file(onto, fName_save) 
             
        print("\n")
        file_modification.save_in_file(onto, fName_save)
        return unnested_classes

def nest_classes_deeper(onto, jump, previous_jump, fName_save):
    from createOnto import get_superclasses, check_inheritance_cycle
    from userInput import query_yes_no
    from autocomplete import suggest_stuff
               
    # if at top level
    if jump == "Thing":
        unnested_classes = []
        for c in list(onto.classes()):
            # no superclasses
            if not get_superclasses(onto, c.name, True):
                unnested_classes.append(c.name)
    
    else:
        unnested_classes = [c.name for c in onto[jump].subclasses()]
        
    jump_classes = set(return_classes_with_subclasses(onto, unnested_classes))
    jump_classes = list(jump_classes)
    # still classes to jump to
    if jump_classes:
        print("Those are subclasses of %s that have further subclasses: \n" % jump)
        print(sorted(jump_classes)) 
            
        previous_jump = jump
        jump = suggest_stuff(jump_classes, "\nEnter the concept you want to jump into: ")
            
        if jump == "exit":
            raise SystemExit
        elif jump not in jump_classes:
            print("The concept you entered does not exist.")
            # set jump back
            jump = previous_jump
        else:                  
            # recursive call, nest classes and go deeper
            print("\n>>> Moving into %s" % jump)
            
            not_done = True
            while not_done:
                unnested_classes = [c.name for c in onto[jump].subclasses()]
                print("\nThese are all concepts that are subclasses of %s." % jump) 
                print(unnested_classes)
                if query_yes_no("\nDo you want to nest concepts on the current level?"):
                    nest_classes_at_level(onto, fName_save, jump, unnested_classes)
                
                jump_classes = return_classes_with_subclasses(onto, unnested_classes)
                # still classes to jump to
                if jump_classes:
                    if query_yes_no("\nDo you want to go to a deeper level to do further nesting?"):
                        nest_classes_deeper(onto, jump, previous_jump, fName_save)
                    else:
                        print("\n<<< Moving out of %s and back into %s" % (jump, previous_jump))
                        not_done = False
                        break
                else:
                    print("\n<<< Moving out of %s and back into %s" % (jump, previous_jump))
                    not_done = False
                    
            
# returns all classes from list class_list that have subclasses
def return_classes_with_subclasses(onto, class_list):
    jump_classes = []
    with onto:
        for c in class_list:
            if onto[c].subclasses():
                jump_classes.append(c)        
    return jump_classes
    
# remove the explicit mention of subclass of owl.Thing that gets added in the dynamic creation 
def remove_Thing(onto):                 
    for s in list(onto.classes()):
        if owl["Thing"] in s.is_a:
            s.is_a.remove(owl["Thing"])
            