# -*- coding: utf-8 -*-

import settings, file_modification
from owlready2 import *
import TMPrel

#==============================================================================
# Calls all functions to build generic relations with user input
# Includes: partOf, Contains
#==============================================================================

def GR(onto):   
    from userInput import query_yes_no
    import relHasPart, relLocation, relCapable
    import help_output
    
    fName_save = settings.fName_GR
    dic_tax_map = file_modification.read_dic_from_file(settings.fName_dic_tax_map)
    dic_dump = file_modification.read_dic_from_file(settings.fName_dic_dump)    
   
    help_output.help_suggestions()    
    help_output.help_relations()
    print("\n")
    
    with onto:
        TMPrel.create_TMPrel(onto)
        
        # if yes is chosen but nothing added, the relation is also not created
        if query_yes_no("Do you want to add a HasPart and PartOf relation?"):
            relHasPart.run_hasPart(onto, dic_tax_map, dic_dump,fName_save)
        if query_yes_no("Do you want to add relations for possible concept locations?"):
            relLocation.run_location(onto, dic_tax_map, dic_dump, fName_save)
        if query_yes_no("Do you want to add relations for possible concept capabilities?"):
            relCapable.run_capable(onto, dic_tax_map, dic_dump, fName_save)
        else:
            pass
        
        file_modification.save_in_file(onto, fName_save)

# extend dic_dump at key c with elements from list dump
def extend_dump(dic_dump, c, dump):
  # if dump is not empty
    if dump:
        # extend to key if exists else create key
        if c.name in dic_dump:
            for e in dump:
                # only add element if it is not in the key yet
                if e not in dic_dump[c.name]:
                    dic_dump[c.name].append(e) 
        else:
            dic_dump[c.name] = dump 

# remove concept itself if it should show up
def remove_c(c, options_list):
    options_list_remove = []
    
    for e in options_list:
        if e[2] == c.name:     
            options_list_remove.append(e)
    
    return [x for x in options_list if x not in options_list_remove]   
    
class ClassQueue:

    def __init__(self):
        from collections import deque

        self.open_list = deque([])
        self.done_list = deque([])
    
    def return_class_queue(self, onto, dic_tax_map):
        self.update_class_queue(onto, dic_tax_map)
        while True:
            if not self.open_list:
                break
            else:
                l = self.open_list.popleft()
                self.update_class_queue(onto, dic_tax_map, l)
        return self.done_list


    def update_class_queue(self, onto, dic_tax_map, l = None):
        import itertools
        from createOnto import get_superclasses
        
        with onto:
            # first call, open list is empty
            if not self.open_list and l is None:
                for c in list(onto.classes()):
                    if c.name in dic_tax_map:
                        # highest level, no ancestors
                        if not get_superclasses(onto, c.name, True):
                            self.open_list.append(c.name)
                                            
            # go through open_list
            elif self.open_list or l is not None:
                self.done_list.append(l)
                
                for x in list(onto[l].subclasses()):
                    if x.name in dic_tax_map:
                        # x occurs lower down in the tree, the direct superclass has not been added yet
                        add = True
                        for y in get_superclasses(onto, x.name, True):
                            if y.name != l and y.name not in self.open_list+self.done_list or x.name in self.open_list:
                                add = False
                                break
                        if add == True:
                            self.open_list.append(x.name)
                            # check for equivalent concepts
                            for e in x.equivalent_to:
                                if e not in self.open_list and e not in self.done_list:
                                    self.open_list.append(e.name)

