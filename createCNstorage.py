# -*- coding: utf-8 -*-
import settings, file_modification
from owlready2 import *
import request, ontoParsing

class CNstorage:
    # rel is the CN relation, phrase the respective for output
    def __init__(self, onto, rel, phrase, load):
        self.rel = rel
        self.phrase = phrase
        self.CN_dic_left = {}
        self.CN_dic_right = {}       
        
        dic_tax_map = file_modification.read_dic_from_file(settings.fName_dic_tax_map)
        
        # only used for testing purposes to not have to requery every run
        if load:
            self.CN_dic_left = file_modification.read_dic_from_file("/home/katinka/Dokumente/Master Thesis/Onto/CN_dic_left_location")
            self.CN_dic_right = file_modification.read_dic_from_file("/home/katinka/Dokumente/Master Thesis/Onto/CN_dic_right_location")
        else:
            self.build_CN_dic(onto, dic_tax_map)
            file_modification.save_dic_in_file(self.CN_dic_left,"/home/katinka/Dokumente/Master Thesis/Onto/CN_dic_left_location")
            file_modification.save_dic_in_file(self.CN_dic_right,"/home/katinka/Dokumente/Master Thesis/Onto/CN_dic_right_location")


    def build_CN_dic(self, onto, dic_tax_map):
        from genericRelations import remove_c
        
        print("[Start building suggestions for %s]" % self.phrase)
        counter_now = 0
        counter_total = len(dic_tax_map)
        
        for c in list(onto.classes()):
            # only if key is in the dictionary (only base taxonomy)
            if c.name in dic_tax_map:
                cid = dic_tax_map[c.name]
                cid = ontoParsing.remove_nountag(cid) # remove nountag if there is one in dict
                
                print("[Getting suggestions for %s (%s/%s)]" % (c.name,counter_now+1, counter_total))
                options_list1, options_list2 = CNstorage.get_relations(self, cid)
                ontoParsing.add_name_oclasses(options_list1)
                ontoParsing.add_name_oclasses(options_list2)
                                
                # only store if not empty
                if options_list2:
                    self.CN_dic_left[c.name] = options_list2
                if options_list1:
                    self.CN_dic_right[c.name] = options_list1
                
                counter_now += 1
                
                
       
    def get_relations(self, cid):
        r1 = request.CNRequest(cid, 3, self.rel) # start = cid
        obj1 = r1.get_obj()
        r2 = request.CNRequest(cid, 4, self.rel) # end = cid
        obj2 = r2.get_obj()
        
        options_list1 = [] # right
        options_list2 = [] # left       
            
        # adds the CN results 
        for e in (obj1['edges']):
            try:
                # language has to match
                if e['start']['language'] == settings.language and e['end']['language'] == settings.language: 
                    x1 = [e['start']['@id'], e['start']['label'], None]
                    x2 = [e['end']['@id'], e['end']['label'], None]
    
                    if e['weight'] >= r1.get_weight() and x2 not in options_list1:
                           options_list1.append(x2)                 
                   
            except KeyError:
                continue
            
        for e in (obj2['edges']):
            try:
                # language has to match
                if e['start']['language'] == settings.language and e['end']['language'] == settings.language: 
                    x1 = [e['start']['@id'], e['start']['label'], None]
                    x2 = [e['end']['@id'], e['end']['label'], None]
    
                    if e['weight'] >= r2.get_weight() and x1 not in options_list2:
                           options_list2.append(x1)
                           
            except KeyError:
                continue
        
        return(options_list1, options_list2)
        
    def print_CNdics(self):
        for key, values in self.CN_dic_left.items():
            print(key, [x[2] for x in values])
        for key, values in self.CN_dic_right.items():
            print(key, [x[2] for x in values])