# -*- coding: utf-8 -*-

from owlready2 import *
import time
import request, settings
from ontoParsing import remove_nountag

def related_check(c1, suggestions_list, dic_tax_map=None):
    suggestions_list_new = []
    
    remove_nountag(c1)
    
    # requests        
    for c2 in suggestions_list:
        c2_new = remove_nountag(c2[0])
        
        try:
            print(c2_new)
            tries = 4
            while tries > 0:
                try:
                    print("tryfirst")
                    r = request.CNRequest(c1, 0, None, None, c2_new)
                    obj = r.get_obj()
                    print(obj)
                    if obj['related']:
                        for rel in (obj['related']):
                            print(rel)
                            #print(obj)
                            if rel['weight']>= settings.numberbatch_threshold:
                                suggestions_list_new.append(c2)
                                tries = 0
                            else:
                                tries = 0
                    else:
                        print("Something could not be checked.")
                        tries = 0
                except (KeyError, ValueError):
                    try:
                        print("trysecond")
                        r = request.CNRequest(c2_new, 0, None, None, c1)
                        obj = r.get_obj()
                        print(obj)
                        for rel in (obj['related']):
                            #print(obj)
                            if rel['weight']>= settings.numberbatch_threshold:
                                suggestions_list_new.append(c2)
                                tries = 0
                            else:
                                tries = 0
                    except (KeyError, ValueError):
                        if tries == 1:
                            raise
                        else:
                            time.sleep(10)
                            tries -= 1
                            print("new try", tries)
            
        except (KeyError, ValueError):
            print("Error deconding JSON.")
            suggestions_list_new.append(c2)
            
    return suggestions_list_new

